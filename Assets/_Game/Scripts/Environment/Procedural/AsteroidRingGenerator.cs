﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SeededGenerator))]
public class AsteroidRingGenerator : MonoBehaviour
{
    public RailsBody focalPoint;
    public int roidCount = 10;
    public float meanSemiMajor = 600;
    public float semiMajorDeviation = 0.03f;
    public float angleDeviation = 2f;
    public AsteroidGeneration.Properties roidProperties;
    public GameObject asteroidPrefab;
    public int ringBackPointCount = 1200;

    private float ringPeriod;
    private SeedRandom randomGenerator;

    public void Generate() {
        if(HasRoids()) {
            if (Application.isEditor) {
                for(int i=transform.childCount-1; i>=0; i--) {
                    GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
                }
            } else {
                foreach(Transform child in transform) {
                    GameObject.Destroy(child);
                }
            }
        }

        randomGenerator = GetComponent<SeededGenerator>().Generator();
        var meanOrbit = new CircularOrbit(meanSemiMajor, 0, focalPoint);
        ringPeriod = meanOrbit.CalcPeriod();
        
        for(int i=0; i<roidCount; i++) {
            float meanOffset = (float)i/(float)roidCount * 360f;
            meanOffset = randomGenerator.Range(meanOffset + angleDeviation, meanOffset - angleDeviation);

            SpawnAsteroid(meanOffset);
        }

        //TODO: GenerateRingMesh();
    }

    public bool HasRoids() {
        // TODO: Might be to lazy? Don't see a reason for any more ring children.
        return transform.childCount >= roidCount;
    }

    private GameObject SpawnAsteroid(float meanOffset) {
        GameObject roidObject = GameObject.Instantiate(asteroidPrefab, transform);
        AsteroidGeneration roidGeneration = roidObject.GetComponent<AsteroidGeneration>();
        roidGeneration.props = roidProperties;

        SeededGenerator roidRandom = roidObject.GetComponent<SeededGenerator>();
        roidRandom.seed = randomGenerator.NextInt().ToString();

        OrbitingBody body = roidObject.GetComponent<OrbitingBody>();
        body.orbit.semiMajor = randomGenerator.Range(meanSemiMajor - meanSemiMajor * semiMajorDeviation, meanSemiMajor + meanSemiMajor * semiMajorDeviation);
        body.orbit.period = ringPeriod;
        body.orbit.meanOffsetDeg = meanOffset;
        body.orbit.focalPoint = focalPoint;
        body.hasOrbitalLine = false;

        roidGeneration.Start();
        body.MoveToStartPos();

        return roidObject;
    }
}
