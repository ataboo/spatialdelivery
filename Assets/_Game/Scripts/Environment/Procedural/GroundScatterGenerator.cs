﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScatterGenerator : MonoBehaviour
{
    public GameObject craterPrefab;

    public Sprite[] lowAngleCraters;
    public Sprite[] midAngleCraters;
    public Sprite[] highAngleCraters;

    public Spacing lowSpacing;
    public Spacing midAngleSpacing;
    public Spacing highAngleSpacing;

    public string randSeedOverride = "";

    private SeedRandom randGenerator;
    private RailsBody body;
    private PolygonCollider2D parentCollider;
    
    public void Generate() {
        if (AlreadySeeded()) {
            if (Application.isEditor) {
                for(int i=transform.childCount-1; i>=0; i--) {
                    GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
                }
            } else {
                foreach(Transform child in transform) {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }

        body = GetComponentInParent<RailsBody>();
        parentCollider = GetComponentInParent<PolygonCollider2D>();

        if (randSeedOverride != "") {
            randGenerator = new SeedRandom(randSeedOverride);
        } else {
            randGenerator = GetComponentInParent<SeededGenerator>().Generator();
        }

        GenerateForAngle(lowAngleCraters, lowSpacing);
        GenerateForAngle(midAngleCraters, midAngleSpacing);
        GenerateForAngle(highAngleCraters, highAngleSpacing);
    }

    private void GenerateForAngle(Sprite[] craters, Spacing spacing) {
        float polarAngle = 0;
        int craterCount = 0;
        
        while(polarAngle < 360f && craterCount < 10000) {
            polarAngle += randGenerator.Range(spacing.polarDegreeRange.x, spacing.polarDegreeRange.y);
            float polarRads = polarAngle * Mathf.Deg2Rad;
            
            float radius = randGenerator.Range(spacing.radiusRange.x, spacing.radiusRange.y) * body.approxRadius;

            Vector3 craterPos = new Vector3(Mathf.Cos(polarRads) * radius, Mathf.Sin(polarRads) * radius, 0) + transform.position; 

            var crater = GameObject.Instantiate(craterPrefab, craterPos, Quaternion.Euler(0, 0, polarAngle-90f));
            crater.transform.parent = transform;

            SpriteRenderer spriteRenderer = crater.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = randGenerator.Element<Sprite>(craters);
            spriteRenderer.flipX = randGenerator.Bool();
            craterCount++;

            if (spacing.cullFloating) {
                var topLeft = crater.transform.TransformPoint(new Vector2(spriteRenderer.sprite.bounds.min.x, spriteRenderer.sprite.bounds.max.y));
                var topRight = crater.transform.TransformPoint(new Vector2(spriteRenderer.sprite.bounds.max.x, spriteRenderer.sprite.bounds.max.y));
                
                if (!parentCollider.OverlapPoint(topLeft) || !parentCollider.OverlapPoint(topRight)) {
                    spriteRenderer.color = Color.green;
                    
                    if (Application.isEditor) {
                        GameObject.DestroyImmediate(crater);
                    } else {
                        GameObject.Destroy(crater);
                    }
                }
            }
        }
    }

    public bool AlreadySeeded() {
        return transform.childCount > 0;
    }



    [System.Serializable]
    public class Spacing {
        // Random range between craters (degrees)
        public Vector2 polarDegreeRange = new Vector2(5, 10);

        // Factor of avg. semi-major axis (0<=range<=1)
        public Vector2 radiusRange = new Vector2(0, 1);
        
        public bool cullFloating = false;
    }
}
