﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SeededGenerator))]
public class AsteroidGeneration : MonoBehaviour
{
    public Properties props;

    // TODO: get rid of me.
    private List<BezierCurve> debugCurves = new List<BezierCurve>();

    private List<Vector3> seedPoints = new List<Vector3>();
    private PolygonCollider2D polyCollider;
    private Rigidbody2D rb;
    private RailsBody orbitalBody;
    private SeedRandom randGen;
    private float maxRadius;
    

    public void Start() {
        randGen = GetComponent<SeededGenerator>().Generator();
        polyCollider = GetComponent<PolygonCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        orbitalBody = GetComponent<RailsBody>();

        GenerateAsteroidMesh();
    }

    public Mesh Mesh() {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter != null) {
            return meshFilter.sharedMesh;
        }

        return null;
    }

    public void MoveToOriginPosition() {
        orbitalBody.transform.position = orbitalBody.PositionAtTime(0);
    }

    // void OnDrawGizmosSelected() {
    //     Gizmos.color = Color.green;
    //     float radius = orbitalBody != null ? orbitalBody.approxRadius : radiusRange.y * (1+radiusDeviation.y);
    //     Gizmos.DrawWireSphere(transform.position, radius);
    // }

    // void OnDrawGizmosSelected() {
        // Gizmos.color = Color.black;
        // for(int i=0; i<seedPoints.Count; i++) {
        //     Vector3 current = seedPoints[i];
        //     Vector3 next = i == seedPoints.Count - 1 ? seedPoints[0] : seedPoints[i+1];
            
        //     Gizmos.DrawLine(current, next);
        // }

        // foreach(BezierCurve curve in debugCurves) {
        //     curve.Draw();
        // }
    // }

    private void GenerateAsteroidMesh() {
        maxRadius = 0f;
        var baseRadius = randGen.Range(props.radiusRange.x, props.radiusRange.y);
        seedPoints = SeedBasePoints(baseRadius);
        orbitalBody.approxRadius = baseRadius;
        orbitalBody.maxRadius = maxRadius;

        var meshPoints = MakeMeshPoints(baseRadius);
        var triangles = MakeTriangles(meshPoints);
        var uvs = MakeUVs(meshPoints, baseRadius);
        polyCollider.points = GetColliderPoints(meshPoints);
        
        Mesh mesh = new Mesh();
        mesh.vertices = meshPoints.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.SetUVs(0, uvs);
        mesh.RecalculateNormals();
        
        var filter = GetComponent<MeshFilter>();
        filter.sharedMesh = mesh;
    }

    private List<Vector3> SeedBasePoints(float baseRadius) {
        int seedPointCount = (int)Mathf.Round(randGen.Range(props.seedPointRange.x, props.seedPointRange.y));
        var seedPoints = new List<Vector3>();
        var radiusDeviationRate = randGen.Range(props.radiusDeviation.x, props.radiusDeviation.y);

        for (int i=0; i<seedPointCount; i++) {
            float angle = 2 * Mathf.PI * (float)i / (float)seedPointCount;
            float radius = randGen.Range(baseRadius - radiusDeviationRate * baseRadius, baseRadius + radiusDeviationRate * baseRadius);  
            maxRadius = Mathf.Max(radius, maxRadius);
            seedPoints.Add(new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0) * radius);
        }

        return seedPoints;
    }

    private List<Vector3> MakeMeshPoints(float baseRadius) {
        debugCurves.Clear();
        float handleLength = baseRadius * props.smoothRate / seedPoints.Count;
        var meshPoints = new List<Vector3>();
        meshPoints.Add(Vector3.zero);

        for(int i=0; i<seedPoints.Count; i++) {
            Vector3 first = seedPoints[i];
            Vector3 second = seedPoints[(i+1) % seedPoints.Count];
            Vector3 third = seedPoints[(i+2) % seedPoints.Count];
            Vector3 fourth = seedPoints[(i+3) % seedPoints.Count];
            
            var curve = new BezierCurve(first, second, third, fourth, handleLength);
            var smoothPoints = curve.PointsOnCurve(props.smoothPointCount);

            debugCurves.Add(curve);

            meshPoints.AddRange(smoothPoints);
        }

        return meshPoints;
    }

    private List<int> MakeTriangles(List<Vector3> meshPoints) {
        var triangles = new List<int>();

        for (int i=0; i<meshPoints.Count; i++) {
            var nextIdx = i==meshPoints.Count - 1 ? 1 : i+1;
            
            triangles.Add(0);
            triangles.Add(nextIdx);
            triangles.Add(i);
        }

        return triangles;
    }

    private List<Vector2> MakeUVs(List<Vector3> meshPoints, float baseRadius) {
        var uvs = new List<Vector2>();
        Vector2 bounds = new Vector2(baseRadius * (1 + props.radiusDeviation.y), baseRadius * (1 + props.radiusDeviation.y));
        float scale = randGen.Range(props.uvScaleRange.x, props.uvScaleRange.y) * baseRadius;
        Vector2 offset = new Vector2(randGen.Range(0f, 1f), randGen.Range(0f, 1f));
        
        foreach(Vector3 meshPoint in meshPoints) {
            Vector2 uv = new Vector2(meshPoint.x/bounds.x, meshPoint.y/bounds.y) * scale + offset;
            uvs.Add(uv);
        }

        return uvs;
    }

    private Vector2[] GetColliderPoints(List<Vector3> meshPoints) {
        // First mesh point is the center, which we don't need.
        var colliderPoints = new Vector2[meshPoints.Count - 1];

        for(int i=1; i<meshPoints.Count; i++) {
            colliderPoints[i-1] = new Vector2(meshPoints[i].x, meshPoints[i].y);
        }

        return colliderPoints;
    }

    [System.Serializable]
    public class Properties {
        public Vector2 seedPointRange = new Vector2(5, 16);
        public float smoothRate = 2f;
        public int smoothPointCount = 10;
        public Vector2 radiusRange = new Vector2(6f, 12f);
        public Vector2 radiusDeviation = new Vector2(.2f, .5f);
        public Vector2 uvScaleRange = new Vector2(1f, 4f);
    }
}
