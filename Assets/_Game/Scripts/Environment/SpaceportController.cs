﻿using System.Collections;
using System.Collections.Specialized;
using UnityEngine;

public class SpaceportController : MonoBehaviour
{
    OrderedDictionary vehicles = new OrderedDictionary();
    RefuelController[] refuelers;

    public void CheckIn(VehicleController vehicle) {
        vehicles.Add(vehicle.gameObject.GetInstanceID(), vehicle);
    }

    public void CheckOut(VehicleController vehicle) {
        vehicles.Remove(vehicle.gameObject.GetInstanceID());
    }

    void Awake() {
        refuelers = GetComponentsInChildren<RefuelController>();
        foreach(RefuelController refueler in refuelers) {
            var vehicle = refueler.SetSpaceport(this);
            if (vehicle != null) {
                CheckIn(vehicle);
            }
        }
    }

    public VehicleController NextVehicle(VehicleController current) {
        if (vehicles.Count <= 1) {
            return null;
        }

        var enumerator = vehicles.GetEnumerator();
        enumerator.Reset();

        while(enumerator.MoveNext()) {
            if (current.gameObject.GetInstanceID() == (int)enumerator.Key) {
                if (enumerator.MoveNext()) {
                    return (VehicleController)enumerator.Value;
                } else {
                    enumerator.Reset();
                    enumerator.MoveNext();
                    return (VehicleController)enumerator.Value;
                }
            }
        }

        Debug.LogError("Current vehicle not found in spaceport");
        return null;
    }
}
