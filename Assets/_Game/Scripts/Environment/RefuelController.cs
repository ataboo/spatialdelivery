﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefuelController : MonoBehaviour
{
    public float refuelRate = 10f;
    public Transform landingPoint;
    private SpaceportController spacePort;
    private VehicleController currentVehicle;

    public SpaceportController SpacePort {
        get {
            return spacePort;
        }
    }

    public bool HasVehicle {
        get {
            return currentVehicle != null;
        }
    }

    public VehicleController SetSpaceport(SpaceportController spacePort) {
        this.spacePort = spacePort;
        return currentVehicle;
    }

    public void CheckIn(VehicleController vehicle) {
        if (SpacePort != null) {
            spacePort.CheckIn(vehicle);
        }

        currentVehicle = vehicle;
    }

    public void CheckOut(VehicleController vehicle) {
        if (SpacePort != null) {
            spacePort.CheckOut(vehicle);
        }

        currentVehicle = null;
    }
}
