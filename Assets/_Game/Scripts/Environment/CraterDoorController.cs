﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraterDoorController : MonoBehaviour, PayloadWarehouse
{
    public RailsBody planet;
    private RotatingBody rotatingPlanet;
    private List<PayloadController> dyingPayloads = new List<PayloadController>();
    private GravitationalController gravController;
    private DeliveryQuestTrigger questTrigger;

    void Start() {
        questTrigger = GetComponent<DeliveryQuestTrigger>();
        gravController = GameObject.FindGameObjectWithTag("SceneControl").GetComponent<GravitationalController>();
        rotatingPlanet = planet.GetComponent<RotatingBody>();
    }

    void FixedUpdate() {
        Vector2 down = (transform.position - planet.transform.position).normalized;

        var trash = new List<PayloadController>();
        foreach(PayloadController payload in dyingPayloads) {
            if (payload == null) {
                trash.Add(payload);
                continue;
            }
            payload.Rigidbody().velocity = Velocity() - 5f * down;
        }

        foreach(PayloadController payload in trash) {
            dyingPayloads.Remove(payload);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
            PayloadController payload = col.GetComponent<PayloadController>();
            if (payload != null) {
                payload.OfferWarehouse(this);
            }
    }

    void OnTriggerExit2D(Collider2D col) {
            PayloadController payload = col.GetComponent<PayloadController>();
            if (payload != null) {
                payload.RevokeWarehouse(this);
            }
    }

    public void TakePayload(PayloadController payload) {
        payload.SetCollidersActive(false);
        payload.AttachToBody(GetComponent<NewtonianBody>());
        dyingPayloads.Add(payload);
        if (questTrigger != null) {
            questTrigger.Trigger(payload.gameObject);
        }
        GameObject.Destroy(payload.gameObject, 3f);
        gravController.RemoveSatellite(payload.GetComponent<SatelliteBody>());

        
    }

    public Vector2 Velocity() {
        return rotatingPlanet.NetVelocityAtPoint(transform.position);
    }
}

public interface PayloadWarehouse {
    void TakePayload(PayloadController payload);
}
