﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class RenderHider : MonoBehaviour
{
    private SpriteRenderer[] renderers;
    public MapIconStats mapIcon = null;
    public bool hideChildren = true;
    public VectorLine orbitalLine;
    public MapLine mapLine;
    public float mapVisibilityRange = -1;

    // Start is called before the first frame update
    void Start()
    {
        FindRenderers();
        GetMapController().AddRenderHider(this);
    }

    private void FindRenderers() {
        if (hideChildren) {
            renderers = GetComponentsInChildren<SpriteRenderer>();
        } else {
            var renderer = GetComponent<SpriteRenderer>();
            if (renderer != null) {
                renderers = new SpriteRenderer[]{GetComponent<SpriteRenderer>()};
            } else {
                renderers = new SpriteRenderer[]{};
            }
        }
    }

    private MapController GetMapController() {
        var mainCam = SceneContext.instance.MainCamController();

        if (mainCam != null) {
            return mainCam.GetComponent<MapController>();
        }

        return null;
    }

    // Enable/disable all sprite renderers
    // Return false if any renderers are disposed.
    public bool ToggleShowRenderers(bool show, int recursion = 0) {
        // Call me recursion paranoid...
        if (recursion > 1) {
            Debug.LogError(name + " RenderHider tried to infinite loop hard!");
            return false;
        }
        
        if (renderers.Length == 0) {
            return false;
        }
        
        foreach(SpriteRenderer renderer in renderers) {
            if (renderer == null) {
                FindRenderers();
                return ToggleShowRenderers(show, recursion+1);
            } else {
                renderer.enabled = show;
            }
        }

        if (mapLine != null) {
            if (show) {
                mapLine.HideLine();
            } else {
                mapLine.ShowLine();
            }
        }

        return true;
    }

    [System.Serializable]
    public class MapIconStats {
        public Sprite sprite;
        [Range(MapController.MinPriority, MapController.MaxPriority)]
        public int priority = 5;
        public bool showArrow = false;
        public bool colorOverriden = false;
        public Color overrideColor = Color.yellow;
    }
}

public interface MapLine {
    void ShowLine();
    void HideLine();
    bool LineIsVisible();
}
