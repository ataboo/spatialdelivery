﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public const int MinPriority = 0;
    public const int MaxPriority = 10;
    
    public Sprite arrowSprite;
    public float arrowPadding = 20;
    public GameObject iconPrefab;
    public Canvas iconCanvas;
    public bool MapOn {
        get {
            return mapOn;
        }
    }
    private bool mapOn = false;

    // Add MapObject ids here then call RemoveGarboIds() to remove them.
    private List<int> garboIds = new List<int>();

    // Empties used to have some icons to be displayed infront of others.
    private GameObject[] priorityHolders;

    private Dictionary<int, MapObject> mapObjects = new Dictionary<int, MapObject>();
    private List<int> iconMapObjIds = new List<int>();

    void Awake() {
        iconCanvas.enabled = false;

        CreatePriorityHolders();
    }

    public void SetMapOn(bool mapOn) {
        this.mapOn = mapOn;

        iconCanvas.enabled = mapOn;
        
        foreach(KeyValuePair<int, MapObject> entry in mapObjects) {
            var mapObj = entry.Value;
        
            // Returns false if this mapObj has been disposed.
            if (!mapObj.ToggleMap(mapOn)) {
                garboIds.Add(entry.Key);
            }
        }

        RemoveGarboIds();
    }

    void LateUpdate()
    {
        if (!mapOn) {
            return;
        }

        MoveIconsToScreenPos();
    }

    void MoveIconsToScreenPos() {
        Rect screenRect = new Rect(new Vector2(arrowPadding, arrowPadding), new Vector2(Screen.width - 2*arrowPadding, Screen.height - 2*arrowPadding));

        VehicleController currentVehicle = SceneContext.instance.PlayerVehicle();
        if (currentVehicle == null) {
            return;
        }

        Vector2 playerPos = currentVehicle.transform.position;

        foreach(int id in iconMapObjIds) {
            var obj = mapObjects[id];

            if (obj.hider == null || obj.hider.gameObject == null) {
                garboIds.Add(id);
            } else {
                if (obj.icon.gameObject.activeSelf) {
                    Vector2 screenPos = SceneContext.instance.MainCamera().WorldToScreenPoint(obj.hider.transform.position);
                    obj.icon.transform.position = screenPos;

                    if (obj.hider.mapIcon.showArrow) {
                        if(screenRect.Contains(obj.icon.transform.position)) {
                            obj.icon.sprite = obj.hider.mapIcon.sprite;
                            obj.icon.transform.rotation = Quaternion.Euler(0, 0, 0);       
                        } else {
                            obj.icon.sprite = arrowSprite;
                            float bearing = Vector2.SignedAngle(Vector2.up, (screenPos - new Vector2(Screen.width / 2, Screen.height / 2)));
                            Vector2 min = new Vector2(arrowPadding, arrowPadding);
                            Vector2 max = new Vector2(Screen.width - arrowPadding, Screen.height - arrowPadding);
                            obj.icon.transform.position = AtaMath.ClampVector2(obj.icon.transform.position, screenRect);
                            obj.icon.transform.rotation = Quaternion.Euler(0, 0, bearing);
                        }
                    }
                }

                
            }

            if (obj.hider.mapVisibilityRange >= 0) {
                float deltaPlayerMagSqr = ((Vector2)obj.hider.transform.position - playerPos).sqrMagnitude;
                obj.icon.gameObject.SetActive(obj.hider.mapVisibilityRange * obj.hider.mapVisibilityRange >= deltaPlayerMagSqr);
            }
        }

        RemoveGarboIds();
    }

    void RemoveGarboIds() {
        if (garboIds.Count == 0) {
            return;
        }

        foreach(int id in garboIds) {
            RemoveHider(id);
        }

        garboIds.Clear();
    }

    public void AddRenderHider(RenderHider hider) {
        Image iconImg = null;

        if (hider.mapIcon != null && hider.mapIcon.sprite != null) {
            var priorityHolder = GetPriorityHolder(hider.mapIcon.priority);
            GameObject icon = GameObject.Instantiate(iconPrefab, priorityHolder);
            iconImg = icon.GetComponent<Image>();
            iconImg.sprite = hider.mapIcon.sprite;

            if (hider.mapIcon.colorOverriden) {
                iconImg.color = hider.mapIcon.overrideColor;
            } else {
                UnitController unit = hider.GetComponent<UnitController>();
                if (unit != null) {
                    iconImg.color = unit.Faction.iconColor;
                } else {
                    Debug.LogWarningFormat("{0} should probably get a unit controller or override their icon color", hider.name);
                }
            }
        }

        var id = hider.gameObject.GetInstanceID();

        var mapObj = new MapObject(hider, iconImg);
        mapObjects[id] = mapObj;
        if (mapObj.icon != null) {
            iconMapObjIds.Add(id);
        }

        mapObj.ToggleMap(mapOn);
    }

    private void CreatePriorityHolders() {
        priorityHolders = new GameObject[MaxPriority+1];

        for (int i=MinPriority; i<=MaxPriority; i++) {
            var priority = new GameObject(string.Format("IconPriority-{0}", i));
            priority.transform.parent = iconCanvas.transform;
            priority.transform.position = Vector2.zero;
            priorityHolders[i] = priority;
        }
    }

    private Transform GetPriorityHolder(int priority) {
        priority = Mathf.Clamp(priority, MinPriority, MaxPriority);
        return priorityHolders[priority].transform;
    }

    public void RemoveHider(int id) {
        var obj = mapObjects[id];
        if (obj.icon != null) {
            GameObject.Destroy(obj.icon);
            iconMapObjIds.Remove(id);
        }

        mapObjects.Remove(id);
    }

    // Keep reference to a Sprite GameObject (hider) and an icon representation GameObject (icon).
    public class MapObject {
        public RenderHider hider;
        public Image icon;

        public MapObject(RenderHider hider, Image icon) {
            this.hider = hider;
            this.icon = icon;
        }

        public bool ToggleMap(bool mapOn) {
            if (hider == null) {
                return false;
            }
 
            return hider.ToggleShowRenderers(!mapOn);
        }
    }
}
