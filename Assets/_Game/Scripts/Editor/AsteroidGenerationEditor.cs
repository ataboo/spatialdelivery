﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AsteroidGeneration))]
public class AsteroidGenerationEditor : Editor
{
    void OnEnable() {
        var asteroid = target as AsteroidGeneration;

        if (asteroid.Mesh() == null) {
           InitAsteroid(asteroid);
        }
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var asteroid = target as AsteroidGeneration;

        if (GUILayout.Button("Regenerate Mesh")) {
            InitAsteroid(asteroid);
        }
    }

    void InitAsteroid(AsteroidGeneration asteroid) {
        asteroid.Start();
    } 

}
