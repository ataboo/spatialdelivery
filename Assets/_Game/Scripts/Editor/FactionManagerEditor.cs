﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(FactionManager))]
public class FactionManagerEditor : Editor
{
    public override void OnInspectorGUI() {
        serializedObject.Update();
        DrawDefaultInspector();

        var factionManager = (FactionManager)target;
        

        serializedObject.ApplyModifiedProperties();
    }

    // void DrawColorOverrides(FactionManager factionManager) {
    //     FactionEnum[] allFactionEnums = (FactionEnum[])Enum.GetValues(typeof(FactionEnum));

    //     foreach(FactionEnum factionEnum in allFactionEnums) {

    //     }
    // }
}
