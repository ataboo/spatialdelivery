using UnityEngine;
using UnityEditor;


public class VehicleControllerEditor: Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var vehicle = (VehicleController)target;

        if (vehicle.startSittingOn != null) {
            RefuelController refuel = vehicle.startSittingOn.GetComponent<RefuelController>();
            if (refuel != null) {
                if (GUILayout.Button("Move To Refuel")) {
                    vehicle.MoveToLandingPoint(refuel.landingPoint.transform);
                }
            }
        }
        
    }
}

[CustomEditor(typeof(TruckController))]
public class TruckControllerEditor: VehicleControllerEditor {
    //
}

[CustomEditor(typeof(ForkliftController))]
public class ForkliftControllerEditor: VehicleControllerEditor {
    //
}