using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using AtaBehaviour;
using System.Text;


public class BehaviourDebug: EditorWindow
{
    int runnerIndex;
    IBehaviourRunner[] behaviourRunners;
    string[] runnerNames;

    IBehaviourRunner SelectedRunner {
        get {
            if (behaviourRunners.Length <= runnerIndex) {
                return null;
            }

            return behaviourRunners[runnerIndex];
        }
    }

    [MenuItem("Debugging/AtaBehaviour")]
    static void Init() {
        BehaviourDebug window = (BehaviourDebug)GetWindow(typeof(BehaviourDebug));
        window.Show();
    }

    public void OnGUI() {
        if (behaviourRunners == null || (behaviourRunners.Length > 0 && SelectedRunner == null)) {
            FindPilots();
        }

        if (GUILayout.Button("Find Pilots")) {
            FindPilots();
        }
        runnerIndex = EditorGUILayout.Popup("Select Pilot", runnerIndex, runnerNames);
        
        GUIStyle style = new GUIStyle(GUI.skin.label);
        style.fontStyle = FontStyle.Bold;
        style.richText = true;

        EditorGUILayout.BeginVertical();

        if (SelectedRunner == null) {
            GUILayout.Label("Select a GameObject with an Autopilot Component", style);
            return;
        }

        if (SelectedRunner.CurrentBehaviour() == null) {
            GUILayout.Label(string.Format("No active behaviour tree in {0}", SelectedRunner.gameObject.name), style);
            return;
        }

        LogRow logRow = new LogRow(SelectedRunner.CurrentBehaviour(), new NodeColors());

        logRow.RenderGUI(style);

        EditorGUILayout.EndVertical();
    }

    public void OnInspectorUpdate()
    {
        this.Repaint();
    }

    void FindPilots() {
        var autoPilots = GameObject.FindObjectsOfType<Autopilot>();
        var surfaceGunners = GameObject.FindObjectsOfType<SurfaceGunner>();
        
        behaviourRunners = new IBehaviourRunner[autoPilots.Length + surfaceGunners.Length];
        for(int i=0; i<autoPilots.Length; i++) {
            behaviourRunners[i] = autoPilots[i];
        }

        for(int i=0; i<surfaceGunners.Length; i++) {
            behaviourRunners[i+autoPilots.Length] = surfaceGunners[i];
        }

        runnerNames = new string[behaviourRunners.Length];

        for(int i=0; i<behaviourRunners.Length; i++) {
            runnerNames[i] = behaviourRunners[i].gameObject.name;
        }
    }

    public class LogRow {
        int depth;
        int row;
        string nodeType;
        List<LogRow> childrenRows;
        Node.Result result;
        Node node;
        BehaviourDebug.NodeColors colors;

        public LogRow(Node node, BehaviourDebug.NodeColors colors=null, int depth=0, int row = 0) {
            this.node = node;
            this.nodeType = node.GetType().ToString();
            this.depth = depth;
            this.row = row;
            this.colors = colors;
            result = node.LoggedResult();
            childrenRows = new List<LogRow>();
            if (node.Children() != null) {
                foreach(Node child in node.Children()) {
                    row++;
                    childrenRows.Add(new LogRow(child, colors, depth+1, row));
                }
            }
        }

        public void RenderGUI(GUIStyle style) {
            if (result == Node.Result.Running) {
                style.normal.background = colors.backWhite;
            } else {
                style.normal.background = colors.backLightGray; 
            }

            EditorGUI.indentLevel = depth;
            EditorGUILayout.LabelField(ToString(), style);
            foreach(LogRow child in childrenRows) {
                child.RenderGUI(style);
            }
        }

        public string ToStringRecursive() {
            var builder = new StringBuilder();
            builder.Append(new string(' ', 2*depth));
            builder.Append(ToString());

            foreach(LogRow child in childrenRows) {
                builder.Append("\n");
                builder.Append(child.ToStringRecursive());
            }

            return builder.ToString();
        }

        public override string ToString() {
            var builder = new StringBuilder();
            builder.Append(nodeType);
            builder.Append(" - ");
            builder.Append(result);
            if (!node.HasContext()) {
                builder.Append(" (No CTX!)");
            }
            
            string content = builder.ToString();

            if (colors != null) {
                return ColoredText(content, GetColor());
            } else {
                return content;
            }
        }

        public List<LogRow> Children() {
            return childrenRows;
        }

        string ColoredText(string content, Color color) {
            return string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(color), content);
        }

        Color GetColor() {
            switch(this.result) {
                case Node.Result.Failure:
                    return colors.fail;
                case Node.Result.Running:
                    return colors.running;
                case Node.Result.Success:
                    return colors.success;
                default:
                    return colors.unset;
            }
        }
    }

    public class NodeColors {
        public Color32 success = new Color32(0x1C, 0x54, 0x42, 0xFF);
        public Color32 running = new Color32(0x0A, 0x54, 0x71, 0xFF);
        public Color32 fail = new Color32(0xC0, 0x35, 0x35, 0xFF);
        public Color32 unset = new Color32(0xFD, 0x5F, 0x00, 0xFF);

        public Texture2D backWhite = new Texture2D(1, 1);
        public Texture2D backLightGray = new Texture2D(1, 1);

        public NodeColors() {
            backWhite.SetPixel(0, 0, Color.white);
            backWhite.Apply();

            backLightGray.SetPixel(0, 0, new Color(0.85f, 0.85f, 0.85f, 1f));
            backLightGray.Apply();
        }
    }
}
