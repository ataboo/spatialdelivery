using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

abstract public class RailsBodyEditor : Editor
{
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var body = target as RailsBody;

        if (GUILayout.Button("To Start Position")) {
            body.MoveToStartPos();
        }
    }
}

[CustomEditor(typeof(OrbitingBody))]
public class OrbitingBodyEditor : RailsBodyEditor {
    //
}

[CustomEditor(typeof(StationaryBody))]
public class StationaryBodyEditor : RailsBodyEditor {
    //
}
