﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AsteroidRingGenerator))]
public class AsteroidRingGenerationEditor : Editor
{
    void OnEnable() {
        var ringGenerator = target as AsteroidRingGenerator;

        if (!ringGenerator.HasRoids()) {
           ringGenerator.Generate();
        }
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var ringGenerator = target as AsteroidRingGenerator;

        if (GUILayout.Button("Generate Asteroids")) {
            ringGenerator.Generate();
        }
    }
}
