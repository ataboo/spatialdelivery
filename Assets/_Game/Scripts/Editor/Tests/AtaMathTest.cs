﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class AtaMathTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void NormalizeAngles()
        {
            var table = new NormalizeRow[]{
                new NormalizeRow(0, 0),
                new NormalizeRow(360, 0),
                new NormalizeRow(180, 180),
                new NormalizeRow(-180, 180),
                new NormalizeRow(-1, 359),
                new NormalizeRow(-361, 359),
                new NormalizeRow(-721, 359),
                new NormalizeRow(361, 1),
                new NormalizeRow(721, 1)
            };
            
            foreach(NormalizeRow row in table) {
                row.AssertRow();
            }
        }

        [Test]
        public void AngleInRange() {
            var table = new AngleRangeRow[]{
                new AngleRangeRow(0, 0, 180, true),
                new AngleRangeRow(360, 0, 180, true),
                new AngleRangeRow(-180, 0, 180, true),
                new AngleRangeRow(359, 0, 180, false),
                new AngleRangeRow(181f, 0, 180, false),
                new AngleRangeRow(-1, 0, 180, false),
                new AngleRangeRow(-361, 0, 180, false),
                new AngleRangeRow(-179, 0, 180, false),
                new AngleRangeRow(-720, 0, 180, true),
                new AngleRangeRow(-721, 0, 180, false),
                new AngleRangeRow(359, 0, 180, false),

                new AngleRangeRow(0, 270, 90, true),
                new AngleRangeRow(270, 270, 90, true),
                new AngleRangeRow(90, 270, 90, true),
                new AngleRangeRow(91, 270, 90, false),
                new AngleRangeRow(269, 270, 90, false),
                new AngleRangeRow(-91, 270, 90, false),
                new AngleRangeRow(-90, 270, 90, true),
                
                new AngleRangeRow(0, -90, -270, true),
                new AngleRangeRow(270, -90, -270, true),
                new AngleRangeRow(90, -90, -270, true),
                new AngleRangeRow(91, -90, -270, false),
                new AngleRangeRow(269, -90, -270, false),
            };

            foreach(AngleRangeRow row in table) {
                row.AssertRow();
            }
        }

        [Test]
        public void DeltaAngle() {
            var table = new DeltaAngleRow[] {
                new DeltaAngleRow(0, 30, 30, 30),
                new DeltaAngleRow(30, 0, -30, 30),
                new DeltaAngleRow(330, 30, 60, 60),
                new DeltaAngleRow(30, 330, -60, 60),

                new DeltaAngleRow(-30, 30, 60, 60),
                new DeltaAngleRow(30, -30, -60, 60),
                new DeltaAngleRow(-30, -40, -10, 10),
                new DeltaAngleRow(-40, -30, 10, 10)
            };

            foreach(DeltaAngleRow row in table) {
                row.AssertRow();
            }
        }

        [Test]
        public void ClampAngle() {
            var table = new ClampAngleRow[]{
                new ClampAngleRow(0, 0, 180, 0),
                new ClampAngleRow(360, 0, 180, 0),
                new ClampAngleRow(-180, 0, 180, 180),
                new ClampAngleRow(359, 0, 180, 0),
                new ClampAngleRow(181f, 0, 180, 180),
                new ClampAngleRow(-1, 0, 180, 0),
                new ClampAngleRow(-361, 0, 180, 0),
                new ClampAngleRow(-179, 0, 180, 180),
                new ClampAngleRow(-720, 0, 180, 0),
                new ClampAngleRow(-721, 0, 180, 0),
                new ClampAngleRow(359, 0, 180, 0),

                new ClampAngleRow(0, 270, 90, 0),
                new ClampAngleRow(270, 270, 90, 270),
                new ClampAngleRow(90, 270, 90, 90),
                new ClampAngleRow(91, 270, 90, 90),
                new ClampAngleRow(269, 270, 90, 270),
                new ClampAngleRow(-91, 270, 90, 270),
                new ClampAngleRow(-90, 270, 90, 270),
                new ClampAngleRow(181, 270, 90, 270),
                new ClampAngleRow(179, 270, 90, 90),
                new ClampAngleRow(180, 270, 90, 90),

                new ClampAngleRow(0, -270, -90, 270),
                new ClampAngleRow(270, -270, -90, 270),
                new ClampAngleRow(90, -270, -90, 90),
                new ClampAngleRow(89, -270, -90, 90),
                new ClampAngleRow(271, -270, -90, 270),
            };

            foreach(ClampAngleRow row in table) {
                row.AssertRow();
            }
        }

        struct NormalizeRow {
            public float input;
            public float expected;

            public NormalizeRow(float input, float expected) {
                this.input = input;
                this.expected = expected;
            }

            public void AssertRow() {
                Assert.AreEqual(expected, AtaMath.NormalizeAngle(input), "Input: {0}", input);
            }
        }

        struct AngleRangeRow {
            public float value;
            public float lower;
            public float upper;
            public bool expected;
            
            public AngleRangeRow(float value, float lower, float upper, bool expected) {
                this.value = value;
                this.lower = lower;
                this.upper = upper;
                this.expected = expected;
            }

            public void AssertRow() {
                Assert.AreEqual(expected, AtaMath.AngleInRange(value, lower, upper), "Val: {0}, Lower: {1}, Upper: {2}, Expected: {3}", value, lower, upper, expected);
            }
        }

        struct DeltaAngleRow {
            public float a;
            public float b;
            public float expectedSigned;
            public float expectedUnsigned;

            public DeltaAngleRow(float a, float b, float expectedSigned, float expectedUnsigned) {
                this.a = a;
                this.b = b;
                this.expectedSigned = expectedSigned;
                this.expectedUnsigned = expectedUnsigned;
            }

            public void AssertRow() {
                Assert.AreEqual(expectedSigned, AtaMath.DeltaAngleSigned(a, b, true), "a: {0}, b: {1}, signed: {2}", a, b, expectedSigned);
                Assert.AreEqual(expectedUnsigned, AtaMath.DeltaAngle(a, b, true), "a: {0}, b: {1}, unsigned: {2}", a, b, expectedUnsigned);
            } 
        }

        struct ClampAngleRow {
            public float value;
            public float lower;
            public float upper;
            public float expected;

            public ClampAngleRow(float value, float lower, float upper, float expected) {
                this.value = value;
                this.lower = lower;
                this.upper = upper;
                this.expected = expected;
            }

            public void AssertRow() {
                Assert.AreEqual(expected, AtaMath.ClampAngle(value, lower, upper), "Val: {0}, Lower: {1}, Upper: {2}, Expected: {3}", value, lower, upper, expected);
            }
        }
    }
}
