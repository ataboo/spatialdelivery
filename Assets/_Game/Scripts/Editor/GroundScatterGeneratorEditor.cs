﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GroundScatterGenerator))]
public class GroundScatterGeneratorEditor : Editor
{
    void OnEnable() {
        var generator = target as GroundScatterGenerator;

        if (!generator.AlreadySeeded()) {
           generator.Generate();
        }
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var ringGenerator = target as GroundScatterGenerator;

        if (GUILayout.Button("Generate Ground Scatter")) {
            ringGenerator.Generate();
        }
    }
}
