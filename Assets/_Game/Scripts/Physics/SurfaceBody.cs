using UnityEngine;

public class SurfaceBody : PhysicsBody
{
    public RotatingBody parentRotating;
    private NewtonianBody parentNewtonianBody;

    void Awake() {
        parentRotating = GetComponentInParent<RotatingBody>();
        parentNewtonianBody = parentRotating.GetComponent<NewtonianBody>();

        if (parentRotating == null || parentNewtonianBody == null) {
            Debug.LogErrorFormat("SurfaceBody {0} needs a rotating, newtonian parent.");
            this.enabled = false;
        }
    }

    public override Rigidbody2D Rigidbody() {
        if (parentNewtonianBody == null) {
            Awake();
        }

        return parentNewtonianBody.Rigidbody();
    }

    public override NewtonianBody OrbitCenter()
    {
        return parentNewtonianBody.OrbitCenter();
    }

    public override Vector2 Velocity()
    {
        return parentRotating.NetVelocityAtPoint(transform.position);
    }

    public override float Mass() {
        return Rigidbody().mass;
    }
}