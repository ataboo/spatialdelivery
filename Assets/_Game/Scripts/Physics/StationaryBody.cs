using UnityEngine;

public class StationaryBody: RailsBody {
    override public Vector2 PositionAtTime(double time) {
        return transform.position;
    }

    override public Vector2 Velocity() {
        return Vector2.zero;
    }

    public override Vector2 VelocityAtTime(double time)
    {
        return Vector2.zero;
    }

    public override NewtonianBody OrbitCenter() {
        return null;
    }
}