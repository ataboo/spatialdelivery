using UnityEngine;


[RequireComponent(typeof(NewtonianBody))]
abstract public class RotatingBody: MonoBehaviour {
    protected NewtonianBody newtonianBody;
    protected Rigidbody2D rb;

    public abstract float NextRotation();

    void Awake() {
        rb = GetNewtonianBody().Rigidbody();
    }

    void FixedUpdate() {
        rb.angularVelocity = -Mathf.DeltaAngle(NextRotation(), rb.rotation) / Time.fixedDeltaTime;
        rb.rotation %= 360f;
    }

    protected NewtonianBody GetNewtonianBody() {
        if (newtonianBody == null) {
            newtonianBody = GetComponent<NewtonianBody>();
        }

        return newtonianBody;
    }

    public Vector2 NetVelocityAtPoint(Vector2 point) {
        var relativePosition = point - (Vector2)transform.position;
        float radius = relativePosition.magnitude;
        var direction = relativePosition.normalized;

        var progradeNormal = new Vector2(-direction.y, direction.x) * rb.angularVelocity * Mathf.PI / 180f * radius;

        var net = progradeNormal + newtonianBody.Velocity();

        return net;
    }
}