using UnityEngine;
using System.Collections.Generic;
using Vectrosity;

public class OrbitingBody : RailsBody {
    private Vector2 velocity;
    public CircularOrbit orbit;
    public bool hasOrbitalLine = true;
    
    void Start() {
        Rigidbody().position = PositionAtTime(SceneContext.GameTime);

        if (hasOrbitalLine) {
            var renderHider = GetComponent<RenderHider>();
            if (renderHider != null) {
                renderHider.mapLine = orbit;
            }
        }
    }

    void FixedUpdate() {    
        UpdatePosition();
    }

    public virtual void UpdatePosition() {
        // If I just make the asteroid kinematic and update pos, the bouncing is broken.  
        // Better to update velocity and crank up the simulated mass for collisions.
        var newPos = PositionAtTime(SceneContext.GameTime);
        velocity = (newPos - Rigidbody().position) / Time.fixedDeltaTime;

        GetComponent<Rigidbody2D>().velocity = velocity;
    }

    override public Vector2 PositionAtTime(double time) {
        return orbit.PositionAtTime(time);
    }

    override public Vector2 Velocity() {
        return velocity;
    }

    public override Vector2 VelocityAtTime(double time)
    {
        return orbit.VelocityAtTime(time);
    }

    public override NewtonianBody OrbitCenter()
    {
        return orbit.focalPoint;
    }
}