using UnityEngine;
using System.Collections.Generic;
using Vectrosity;
using System;

[Serializable]
public class CircularOrbit: MapLine {
    public float semiMajor;
    public bool clockwise = false;

    public float period = -1;
    public float Period {
        get {
            if (period <= 0) {
                period = CalcPeriod();
            }

            return period;
        }
    }

    public RailsBody focalPoint;

    // Angle from X-axis (radians) at t(0)
    public float meanOffsetDeg;

    public int orbitalPointCount = 100;
    private VectorLine orbitalLine;

    public CircularOrbit(float semiMajor, float meanOffsetDeg, RailsBody focalPoint, bool clockwise=false) {
        this.semiMajor = semiMajor;
        this.meanOffsetDeg = meanOffsetDeg;
        this.focalPoint = focalPoint;
    }

    // Make sure to call this when changing focalPoint or semiMajor
    public float CalcPeriod() {
        // T² = 4𝜋²a³ / Gm
        return Mathf.Sqrt((4 * Mathf.PI * Mathf.PI * Mathf.Pow(this.semiMajor, 3)) / (SharedContext.GRAV_CONST * focalPoint.Mass()));
    }

    private float MeanAtTime(double time) {
        return (float)(time / Period) * Mathf.PI * 2 + meanOffsetDeg * Mathf.Deg2Rad; 
    }

    public Vector2 OffsetAtMean(float meanRads) {
        float x = Mathf.Cos(meanRads);
        float y = (clockwise ? -1f : 1f) * Mathf.Sin(meanRads);

        return new Vector2(x, y) * semiMajor;
    }

    public float VelocityMagnitude() {
        return Mathf.Sqrt(SharedContext.GRAV_CONST * focalPoint.Mass() / semiMajor);
    }

    public Vector2 VelocityAtTime(double time) {
        return focalPoint.VelocityAtTime(time) + LocalVelocityAtTime(time);
    }

    Vector2 LocalVelocityAtTime(double time) {
        return (PositionAtTime(time - Time.fixedDeltaTime) - PositionAtTime(time)) / Time.fixedDeltaTime;
    }

    public Vector2 PositionAtTime(double time) {
        float mean = MeanAtTime(time);
        var position = focalPoint.PositionAtTime(time) + OffsetAtMean(mean);

        return position;
    }

    public void HideOrbitalLine() {
        orbitalLine.color = Color.clear;
        orbitalLine.StopDrawing3DAuto();
    }

    public void ShowLine()
    {
        if (orbitalLine == null) {
            InitLine();
        }
        
        orbitalLine.Draw3DAuto();
        orbitalLine.color = Color.blue;
    }

    public void HideLine()
    {
        if (orbitalLine == null) {
            InitLine();
        }

        orbitalLine.StopDrawing3DAuto();
        orbitalLine.color = Color.clear;
    }

    public bool LineIsVisible()
    {
        return orbitalLine != null && orbitalLine.color != Color.clear;
    }

    private void InitLine() {
        var visualPoints = new List<Vector3>();

        // TODO: This will need to be time based if sub-sub orbits are a thing.
        for(int i=0; i<orbitalPointCount; i++) {
            float t = (float)i / (float) (orbitalPointCount - 1) *  (float)Period;
            visualPoints.Add(PositionAtTime(t));
        }

        orbitalLine = new VectorLine("orbitalline", visualPoints, 1f);
        orbitalLine.lineType = LineType.Continuous;
    }
}