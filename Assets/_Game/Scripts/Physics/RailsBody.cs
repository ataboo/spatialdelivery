﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


abstract public class RailsBody : NewtonianBody
{
    public float approxRadius = 0;
    public float mass = 1000;
    public float cullDistanceSqr = -1;
    public float maxRadius = 0;
    public float collisionRadiusBuffer = 5f;

    override public float Mass() {
        return mass;
    }

    override public float CollisionRadius() {
        return maxRadius + collisionRadiusBuffer;
    }

    public void MoveToStartPos() {
        Vector2 pos = PositionAtTime(0);
        transform.position = new Vector3(pos.x, pos.y, transform.position.z);

        var rotating = GetComponent<RotatingBody>();
        if (rotating != null) {
            transform.rotation = Quaternion.Euler(0, 0, rotating.NextRotation());
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxRadius + collisionRadiusBuffer);
    }

    public abstract Vector2 PositionAtTime(double time);

    public abstract Vector2 VelocityAtTime(double time);
}
