using UnityEngine;

public abstract class PhysicsBody : MonoBehaviour {
    public abstract NewtonianBody OrbitCenter();
    public abstract Vector2 Velocity();
    public abstract Rigidbody2D Rigidbody();
    public abstract float Mass();

    public Vector2 GroundSpeed(RotatingBody otherBody) {
        Vector2 bearing = otherBody.transform.position - transform.position;
        float range = bearing.magnitude;
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, bearing/range, range);

        foreach(RaycastHit2D hit in hits) {
            if (hit.collider.gameObject.GetInstanceID() == otherBody.gameObject.GetInstanceID()) {
                var bodySpeed = otherBody.NetVelocityAtPoint(hit.point);

                // Debug.DrawLine(hit.point - Vector2.up, hit.point + Vector2.up, Color.yellow);
                // Debug.DrawLine(hit.point - Vector2.left, hit.point + Vector2.left, Color.yellow);

                return Velocity() - bodySpeed;
            }
        }

        Debug.LogErrorFormat("failed to get groundspeed over {0}", otherBody.name);
        
        return Vector2.zero;
    }
}