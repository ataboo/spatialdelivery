﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteBody : NewtonianBody
{
    RailsBody orbitCenter;
    NewtonianBody velocityDelegate;
    Vector2 lastPos;
    Vector2 velocity;
    public bool ignoreGravity = false;
    UnitController unit;

    void Start() {
        GameObject.FindGameObjectWithTag("SceneControl").GetComponent<GravitationalController>().AddSatellite(this);
        unit = GetComponent<UnitController>();
        if (unit == null) {
            Debug.LogWarningFormat("Maybe {0} should have a vehicle controller?", name);
        }
    }

    void FixedUpdate() {
        if (velocityDelegate != null) {
            if (lastPos != null) {
                velocity = ((Vector2)transform.position-lastPos) / Time.fixedDeltaTime;
                Rigidbody().velocity = velocity;
            }

            lastPos = Rigidbody().position;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            transform.localPosition = Vector2.zero;
        }
    }

    public void SetOrbitCenter(RailsBody body) {
        this.orbitCenter = body;
    }

    override public NewtonianBody OrbitCenter() {
        return this.orbitCenter;
    }

    override public float CollisionRadius() {
        if (unit == null) {
            Debug.LogWarningFormat("No unit found on: {0}", gameObject.name);
            return 10f;
        }

        return unit.CollisionRadius;
    }

    public void SetVelocityDelegate(NewtonianBody velDel) {
        velocityDelegate = velDel;
        lastPos = transform.position; 
    }

    public NewtonianBody GetVelocityDelegate() {
        return velocityDelegate;
    }

    override public Vector2 Velocity() {
        // if (Rigidbody().bodyType == RigidbodyType2D.Dynamic) {
        //     Debug.Log("returned this RB velocity");
        //     return Rigidbody().velocity;
        // }

        // if (velocityDelegate != null) {
        //     Debug.Log("returned velocity delegate");
        //     return velocity;
        // }

        return Rigidbody().velocity;
    }
}
