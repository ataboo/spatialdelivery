using UnityEngine;


abstract public class NewtonianBody: PhysicsBody {
    private Rigidbody2D rb;

    public override Rigidbody2D Rigidbody() {
        if (rb == null) {
            rb = GetComponent<Rigidbody2D>();
        }

        return rb;
    }

    public abstract float CollisionRadius();

    public override float Mass() {
        return Rigidbody().mass;
    }
}