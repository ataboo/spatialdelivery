using UnityEngine;

[RequireComponent(typeof(SeededGenerator))]
public class ConstantRotatingBody : RotatingBody {
    public Vector2 spinSpeedRange = new Vector2(10f, 20f);
    private float rotationRate;

    public virtual void Start() {
        var randGen = GetComponent<SeededGenerator>().Generator();
        rotationRate = randGen.Range(spinSpeedRange.x, spinSpeedRange.y) * randGen.Sign();
    }

    public override float NextRotation()
    {
        return (float)((double)rotationRate * SceneContext.GameTime % 360d);
    }
}