using UnityEngine;

public class TidalLockRotation : RotatingBody
{
    public float groundAngleDeg = 0f;

    public override float NextRotation()
    {
        var focalPoint = GetNewtonianBody().OrbitCenter();
        rb = GetNewtonianBody().Rigidbody();
        
        if (focalPoint == null) {
            Debug.LogWarningFormat("{0} the tidal locked thing does not have a focal point set!");
            return rb.rotation;
        }

        var horizon = ((Vector2)focalPoint.transform.position - (Vector2)transform.position).normalized;

        float angle =  Mathf.Atan2(horizon.y, horizon.x) * Mathf.Rad2Deg + groundAngleDeg;

        return angle;
    }
}