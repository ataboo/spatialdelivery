using UnityEngine;

public class PayloadController : UnitController {
    public Payload.Type type;
    public Collider2D[] colliders;
    private SatelliteBody satelliteBody;
    private PayloadWarehouse offeredWarehouse;
    private NewtonianBody attachedTo;

    protected override void Awake() {
        base.Awake();

        satelliteBody = GetComponent<SatelliteBody>();
    }

    public void AttachToBody(NewtonianBody newtonianBody) {
        attachedTo = newtonianBody;
        satelliteBody.Rigidbody().bodyType = RigidbodyType2D.Kinematic;
        satelliteBody.Rigidbody().constraints = RigidbodyConstraints2D.FreezeAll;
        satelliteBody.SetVelocityDelegate(newtonianBody);
    }

    public void DetachFromBulkCarrier() {
        if (satelliteBody.GetVelocityDelegate() == null) {
            return;
        }

        attachedTo = null;
        satelliteBody.Rigidbody().bodyType = RigidbodyType2D.Dynamic;
        satelliteBody.Rigidbody().constraints = RigidbodyConstraints2D.None;
        satelliteBody.GetVelocityDelegate().GetComponent<BulkCarrierController>().IgnoreHullCollision(colliders, false);
        transform.parent = null;
        satelliteBody.SetVelocityDelegate(null);
    }

    public void AttachToForklift(NewtonianBody forklift) {
        attachedTo = forklift;
    }

    public void DetachFromForklift(NewtonianBody forklift) {
        attachedTo = null;

        if (offeredWarehouse != null) {
            offeredWarehouse.TakePayload(this);
        }
    }

    public void OfferWarehouse(PayloadWarehouse warehouse) {
        offeredWarehouse = warehouse;

        if (attachedTo == null) {
            offeredWarehouse.TakePayload(this);
        }
    }

    public void RevokeWarehouse(PayloadWarehouse warehouse) {
        offeredWarehouse = null;
    }

    public void SetCollidersActive(bool active) {
        foreach(Collider2D col in colliders) {
            col.enabled = active;
        }
    }

    public Collider2D[] GetColliders() {
        return colliders;
    }

    public Collider2D MainCollider() {
        return colliders[0];
    }

    public Rigidbody2D Rigidbody() {
        return satelliteBody.Rigidbody();
    }
}