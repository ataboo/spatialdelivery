using UnityEngine;
using System;

[Serializable]
public class Payload {
    public enum Type {
        RED_CARGO
    }

    public Type type;

    public GameObject Spawn(Transform spawnPos) {
        return GameObject.Instantiate(Prefab(), spawnPos);
    }

    GameObject Prefab() {
        return SceneContext.instance.GetPayloadPrefab(type);
    }
}