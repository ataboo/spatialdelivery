using UnityEngine;
using UnityEngine.UI;

public class ToastMessage: MonoBehaviour {
    public string message;
    public float lifeSpan;
    public Text textBox;
    public AudioClip audioClip;

    void Start() {
        textBox.text = message;

        if (lifeSpan > 0) {
            GameObject.Destroy(gameObject, lifeSpan);
        }

        if (audioClip != null) {
            SceneContext.instance.MainCamController().PlayToastAudio(audioClip);
        }
    }
}