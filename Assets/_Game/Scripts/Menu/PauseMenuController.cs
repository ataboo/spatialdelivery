﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour
{
    public Button firstSelectedButton;

    void OnEnable() {
        EventSystem es = EventSystem.current;
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(firstSelectedButton.gameObject);
    }
}
