﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void OnStartPushed() {
        SceneManager.LoadScene(SharedContext.Scenes.CargoUnload);
    }

    public void OnExitPushed() {
        SharedContext.Quit();
    }
}
