﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslatedText : MonoBehaviour
{
    public string stringKey;

    void Awake() {
        var textBox = GetComponent<Text>();
        if (stringKey == "") {
            Debug.LogErrorFormat("Forgot to set string key on: {0} with placeholder: {1}", name, textBox.text);
            return;
        }

        textBox.text = SharedContext.GetString(stringKey);

    }
}
