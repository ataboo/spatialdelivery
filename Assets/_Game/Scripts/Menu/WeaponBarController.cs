﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponBarController : MonoBehaviour
{
    public Image turretIcon;
    public Image miningIcon;
    public Color lockedColor = Color.green;
    public Color unlockedColor = Color.yellow;
    public Color offColor = Color.white;
    public Color disabledColor = new Color(0.3f, 0.3f, 0.3f);

    void Awake() {
        turretIcon.color = disabledColor;
        miningIcon.color = disabledColor;
    }

    public void SetTurretState(WeaponController.State state) {
        turretIcon.color = iconColor(state);
    }

    public void SetMiningState(WeaponController.State state) {
        miningIcon.color = iconColor(state);
    }

    Color iconColor(WeaponController.State state) {
        switch(state) {
            case WeaponController.State.Locked:
                return lockedColor;
            case WeaponController.State.Unlocked:
                return unlockedColor;
            case WeaponController.State.Off:
                return offColor;
            default:
                return disabledColor;
        }
    }
}
