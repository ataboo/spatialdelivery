using System;
using UnityEngine;
using System.Collections.Generic;
using System.Text;

namespace Atasoft.Translation {
    public class Translator {
        private string language;
        private Dictionary<string, string> strings;
        private Dictionary<string, QuestData> quests;

        public Translator(string language = "en") {
            this.language = language;

            LoadStrings();
            LoadQuests();
        }

        private void LoadStrings() {
            string rawStringsJson = LoadRawLanguageJson("strings").text;
            var stringList = JsonKeyValueList.FromRawJson(rawStringsJson).stringList;

            strings = ListToDict<JsonKeyValueEntry, string, string>(stringList, (v) => {return v.key;}, (v) => {return v.value;});
        }

        private void LoadQuests() {
            string rawQuestsJson = LoadRawLanguageJson("quests").text;
            var questList = JsonUtility.FromJson<QuestDataList>(rawQuestsJson).questList;

            quests = ListToDict<QuestData, string, QuestData>(questList, (q) => q.id, (q) => q);
        }

        private TextAsset LoadRawLanguageJson(string fileName) {
            string filePath = String.Format("Language/{0}/{1}", language, fileName);
            
            return Resources.Load(filePath) as TextAsset;
        }

        public string GetString(string stringKey) {
            return strings[stringKey];
        }

        public QuestData GetQuest(string questId) {
            return quests[questId];
        }

        private Dictionary<K, V> ListToDict<T, K, V>(List<T> baseList, Func<T, K> keyFunc, Func<T, V> valFunc) {
            var dict = new Dictionary<K, V>();

            foreach(T value in baseList) {
                dict[keyFunc(value)] = valFunc(value);
            }

            return dict;
        }

        [Serializable]
        private struct JsonKeyValueEntry {
            public string key;
            public string value;

            public JsonKeyValueEntry(string key, string value) {
                this.key = key;
                this.value = value;
            }
        }

        [Serializable]
        private struct JsonKeyValueList {
            // Suppress "never assigned to" warning... it's a lie!
            #pragma warning disable 0649
            public List<JsonKeyValueEntry> stringList;
            #pragma warning restore 0649

            public static JsonKeyValueList FromRawJson(string rawJson) {
                return JsonUtility.FromJson<JsonKeyValueList>(rawJson);
            }

            public void SetFromDictionary(Dictionary<string, string> dict) {
                stringList.Clear();
                
                foreach(string key in dict.Keys) {
                    stringList.Add(new JsonKeyValueEntry(key, dict[key]));
                }
            }
        }
    }
}