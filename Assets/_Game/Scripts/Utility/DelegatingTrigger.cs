using UnityEngine;
using System.Collections.Generic;
using System;

public class DelegatingTrigger : MonoBehaviour {
    public Trigger2DDelegate triggerDelegate;
    public Collider2D Collider {
        get {
            return col;
        }
    }

    private Collider2D col;
    
    void Awake() {
        col = GetComponent<Collider2D>();
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (!AssertDelegateSet()) {
            return;
        }

        triggerDelegate.DelTriggerEnter2D(col, this);
    }

    void OnTriggerExit2D(Collider2D col) {
        if (!AssertDelegateSet()) {
            return;
        }

        triggerDelegate.DelTriggerExit2D(col, this);
    }

    bool AssertDelegateSet() {
        if (triggerDelegate == null) {
            Debug.LogErrorFormat("{0} should have a trigger delegate set by the time it reads trigger events.", gameObject.name);
            this.enabled = false;

            return false;
        }

        return true;
    }

    public bool ContainsBody(PhysicsBody body) {
        foreach(Collider2D col in OverlappingColliders()) {
            if (col.GetComponent<PhysicsBody>() == body) {
                return true;
            }
        }

        return false;
    }

    Collider2D[] OverlappingColliders() {
        Collider2D[] overlaps = new Collider2D[16];
        int count = Physics2D.OverlapCollider(col, new ContactFilter2D(), overlaps);

        return new ArraySegment<Collider2D>(overlaps, 0, count).Array;
    }
}

public interface Trigger2DDelegate {
    void DelTriggerEnter2D(Collider2D col, DelegatingTrigger sender);
    void DelTriggerExit2D(Collider2D col, DelegatingTrigger sender);
}