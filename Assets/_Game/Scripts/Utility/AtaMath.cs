using System;
using UnityEngine;

public class AtaMath {
    // Determine if `angle` is between or equal to or between a `lower` and `upper` angle. 
    // Uses degrees
    public static bool AngleInRange(float angle, float lower, float upper, bool normalizeInput = true) {
        if (normalizeInput) {
            lower = NormalizeAngle(lower);
            upper = NormalizeAngle(upper);
            angle = NormalizeAngle(angle);
        }
        
        if (upper < lower) {
            return angle >= lower || angle <= upper; 
        } else {
            return angle <= upper && angle >= lower;
        }
    }   

    // Clamp `angle` between a `lower` and `upper` angle.
    // Returns the closest limit if the provided angle is outside the range.
    // Uses degrees 
    public static float ClampAngle(float angle, float lower, float upper, bool normalizeInput = true) {
        if (normalizeInput) {
            angle = NormalizeAngle(angle);
            lower = NormalizeAngle(lower);
            upper = NormalizeAngle(upper);
        }

        if (AngleInRange(angle, lower, upper)) {
            return angle;
        }

        if (DeltaAngle(angle, lower, false) < DeltaAngle(angle, upper, false)) {
            return lower;
        }
        
        return upper;
    }

    // Get the equivelant to `angle` between 0 and 360 degrees.
    // Uses degrees
    public static float NormalizeAngle(float angle) {
        angle %= 360f;

        if (angle < 0) {
            angle += 360f;
        }

        return angle;
    }

    // Get the difference between two angles unsigned.
    // Uses degrees
    public static float DeltaAngle(float a, float b, bool normalizeInput = true) {
        return Mathf.Abs(DeltaAngleSigned(a, b, normalizeInput));
    }

    // Get the difference between two angles signed.
    // Uses degrees
    public static float DeltaAngleSigned(float a, float b, bool normalize = true) {
        if (normalize) {
            a = NormalizeAngle(a);
            b = NormalizeAngle(b);
        }
        
        return (b - a + 540f) % 360f - 180f;
    }

    public static Vector2Projection Projections(Vector2 v1, Vector2 v2) {
        float scalarProjection = Vector2.Dot(v1, v2) / v2.magnitude;
        Vector2 vectorProjection = scalarProjection * v2.normalized;

        return new Vector2Projection(scalarProjection, vectorProjection);
    }

    public struct Vector2Projection {
        public float scalar;
        public Vector2 vector;

        public Vector2Projection(float scalarProjection, Vector2 vectorProjection) {
            this.scalar = scalarProjection;
            this.vector = vectorProjection;
        }
    }

    // Find the point a bullet should be fired to hit a target.
    // targetV is relative to bullet origin.
    // Returns null if no solution possible.
    public static InterceptSolution CalculateIntercept(PhysicsBody origin, PhysicsBody target, float projectileVelocity) {
        // Hats off to oliii: https://www.gamedev.net/forums/topic/457840-calculating-target-lead/
        // and Jeffrey Hantin: https://stackoverflow.com/questions/2248876/2d-game-fire-at-a-moving-target-by-predicting-intersection-of-projectile-and-u
        
        Vector2 vT = target.Velocity() - origin.Velocity();
        Vector2 originPos = origin.transform.position;
        Vector2 targetPos = target.transform.position;
        Vector2 origToTarg = targetPos - originPos;
        
        // Calculate components and get descriminent for Quadratic Equation. 
        // a = V . V - w * w
        float a = Vector2.Dot(vT, vT) - projectileVelocity * projectileVelocity;
        // b = 2 * V . (P - O)
        float b = 2 * Vector2.Dot(vT, origToTarg);
        // c = (P - O) . (P - O)
        float c = Vector2.Dot(origToTarg, origToTarg);
        
        var solutionsNullable = QuadraticSolutions(a, b, c);
        if (solutionsNullable == null) {
            return null;
        }

        var solutions = solutionsNullable.Value;
        
        float t = LowerPositive(solutions.first, solutions.second, false);
        Vector2 hitPos = (Vector2)target.transform.position + vT * t;

        return new InterceptSolution(t, hitPos);
    }

    // Return the lesser positive of the two values.
    public static float LowerPositive(float a, float b, bool guardBothPositive = true) {
        if (guardBothPositive && a < 0 && b < 0) {
            throw new System.ArgumentException("a nor b is positive.");
        }
        
        if (a < 0) {
            return b;
        }

        if (b < 0) {
            return a;
        }

        return Mathf.Min(a, b);
    }

    // Find the solutions to the Quadratic Equation.
    // ax^2 + bx + c = 0
    public static Nullable<(float first, float second)> QuadraticSolutions(float a, float b, float c, float zeroThreshold = 1E-6f) {
        // Prevent div/0 cases.
        if (Mathf.Abs(a) < zeroThreshold) {
            if (Mathf.Abs(b) < zeroThreshold) {
                if (Mathf.Abs(c) < zeroThreshold) {
                    return (0f, 0f);
                }
                return null;
            }
            return (-c/b, -c/b);
        }

        // discriminant = b^2 - 4ac 
        float d = b*b - 4f*a*c;
        if (d < 0) {
            // No solution.
            return null;
        }

        // x = ( -b +/- sqrt(discriminant) ) / 2a
        float sqrtD = Mathf.Sqrt(d);

        float firstSolution = (-b - sqrtD) / (2f*a);
        float secondSolution = (-b + sqrtD) / (2f*a);

        return (firstSolution, secondSolution);
    }

    public class InterceptSolution {
        public float time;
        public Vector2 hitPos;

        public InterceptSolution(float time, Vector2 hitPos) {
            this.time = time;
            this.hitPos = hitPos;
        }
    }

    public static Vector2 ClampVector2(Vector2 value, Vector2 min, Vector2 max) {
        return new Vector2(Mathf.Clamp(value.x, min.x, max.x), Mathf.Clamp(value.y, min.y, max.y));
    }

    public static Vector2 ClampVector2(Vector2 value, Rect limits) {
        return ClampVector2(value, limits.min, limits.max);
    }
}