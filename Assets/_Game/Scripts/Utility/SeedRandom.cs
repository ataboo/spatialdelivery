﻿using System.Collections;
using System.Collections.Generic;

public class SeedRandom {
    System.Random generator;
    int seedInt;
    string seedString;

    public SeedRandom(string seed) {
        seedString = "";
        if (seed == "") {
            seedInt = (new System.Random()).Next();
        } else {
            int parsed; 
            bool ok = int.TryParse(seed, out parsed);
            if (ok) {
                seedInt = parsed;
            } else {
                seedString = seed;
                seedInt = seed.GetHashCode();
            }
        }

        generator = new System.Random(seedInt);
    }

    public SeedRandom(int seedInt) {
        generator = new System.Random(seedInt);
    }

    public SeedRandom() : this("") {
        //
    }

    public int RangeInt(int lower, int upper) {
        return generator.Next(lower, upper);
    }

    public bool Bool() {
        return NextDouble() > 0.5f;
    }

    public float Range(float lower, float upper) {
        return (float)Range((double)lower, (double)upper);
    }

    public double Range(double lower, double upper) {
        return generator.NextDouble() * (upper - lower) + lower;
    }

    public T Element<T>(T[] elements) {
        if (elements.Length == 0) {
            return default(T);
        }

        return elements[RangeInt(0, elements.Length-1)];  
    }

    public double NextDouble() {
        return generator.NextDouble();
    }

    public int NextInt() {
        return generator.Next();
    }

    public int Sign() {
        return NextDouble() > 0.5d ? 1 : -1;
    }

    public string SeedString() {
        return seedString == "" ? seedInt.ToString() : seedString;
    }

}
