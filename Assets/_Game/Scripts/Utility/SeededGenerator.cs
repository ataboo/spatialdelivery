﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeededGenerator : MonoBehaviour
{
    public string seed = "";
    private SeedRandom generator;
    [ReadOnly] public string seedString;

    public SeedRandom Generator() {
        // if (generator == null) {
            generator = new SeedRandom(seed);
            seedString = generator.SeedString();
        // }

        return generator;
    }
}


 public class ReadOnlyAttribute : PropertyAttribute
 {
     //
 }
