﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve
{
    private float gizmoRadius = .1f;

    Vector2 aPoint;
    Vector2 bPoint;
    Vector2 aHandle;
    Vector2 bHandle;

    // public BezierCurve(Vector2 aPoint, Vector2 bPoint, Vector2 aHandle, Vector2 bHandle) {
    //     this.aPoint = aPoint;
    //     this.bPoint = bPoint;
    //     this.aHandle = aHandle;
    //     this.bHandle = bHandle;
    // }

    public BezierCurve(Vector2 first, Vector2 second, Vector2 third, Vector2 fourth, float handleLength) {
        this.aPoint = second;
        this.bPoint = third;

        Vector2 da = (third - second) + (second - first);
        aHandle = da.normalized * handleLength + second;

        Vector2 db = (third - fourth) + (second - third);
        bHandle = db.normalized * handleLength + third;
    }

    public List<Vector3> PointsOnCurve(int pointCount, float zVal = 0f) {
        var points = new List<Vector3>();
        
        if (pointCount < 2) {
            Debug.LogError("Point count must be greater than 1");
            return points;
        }
        
        for(int i=0; i<pointCount; i++) {
            float t = (float) i/pointCount;
            var curvePoint = PointOnCurve(t);
            points.Add(new Vector3(curvePoint.x, curvePoint.y, zVal));
        }

        return points;
    }

    public void Draw() {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(aPoint, gizmoRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(bPoint, gizmoRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(aHandle, gizmoRadius);
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(bHandle, gizmoRadius);
    }

    private Vector2 PointOnCurve(float t) {
        // B(t) = (1-t)³ P0 + 3(1-t)²t P1 + 3(1-t)t² P2 + t³ P3

        return Mathf.Pow((1f-t), 3) * aPoint + 
            3f * Mathf.Pow((1f-t), 2) * t * aHandle + 
            3f * (1f-t) * Mathf.Pow(t, 2) * bHandle +
            Mathf.Pow(t, 3) * bPoint;
    }
}
