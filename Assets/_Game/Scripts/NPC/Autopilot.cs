﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AtaBehaviour;
using GameBehaviour;

public class Autopilot: MonoBehaviour, IBehaviourRunner
{
    private VehicleController vehicle;
    public NPCBehaviour.Option currentBehaviorOption;
    Node currentBehaviour;
    public RailsBody tetherBody;
    public AnimationCurve hasteCurve;
    
    ActionContext context;
    NavigationCalc navCalc;
    PhysicsBody weaponTarget;

    public bool Engaged {
        get {
            return currentBehaviour != null;
        }
    }

    public NavigationCalc NavCalc {
        get {
            return navCalc;
        }
    }

    void Awake() {
        vehicle = GetComponent<VehicleController>();
        context = new ActionContext(vehicle);
        navCalc = new NavigationCalc(context);
        currentBehaviour = NPCBehaviour.BehaviourFromOption(currentBehaviorOption);
        if (currentBehaviour != null) {
           currentBehaviour.SetContext(ref context);
        }        

        // orbitTree = new Sequence(new Node[] {
        //     new Mute(new TakeOff()),
        //     new Selector(new Node[]{
        //         new DodgeCollision(),
        //         new MoveToOrbit(new CircularOrbit(350f, 0, null))
        //     }),
        //     new Wait(4f)
        // });

        // attackBulkTree = new Sequence(new Node[] {
        //     new NavTargetClosest<BulkCarrierController>(),
        //     new Mute(new TakeOff()),
        //     new Parallel(new Node[] {
        //         new InterceptTargetAndDodge(80f, true),
        //         new Mute(new ShootTarget<BulkCarrierController>()),
        //     }, true),
        //     new Closure((ctx)=>{ctx.Unit.Weapons.StopTracking(); return Node.Result.Success;})
        // });
    }

    void Update() {
        

        // if (Input.GetKeyDown(KeyCode.X)) {
        //     SetCurrentBehaviour(attackTruckTree);
        // }

        // if (Input.GetKeyDown(KeyCode.Z)) {
        //     SetCurrentBehaviour(orbitTree);
        // }

        // if (Input.GetKeyDown(KeyCode.C)) {
        //     SetCurrentBehaviour(null);
        // }

        // if (Input.GetKeyDown(KeyCode.V)) {
        //     SetCurrentBehaviour(attackBulkTree);
        // }

        // SetCurrentBehaviour(attackTruck);

        // if (Input.GetKeyDown(KeyCode.X)) {
        //     var goToX = new Selector(new Node[] {
        //         new TakeOff(),
        //         new Sequence(new Node[] {
        //             new NavTargetClosest<RefuelController>((RefuelController match)=>{
        //                 return !match.HasVehicle;
        //             }),
        //             new WhileElse((ctx)=>{return ctx.collisionDetector.NextCollision != null;},
        //                 new DodgeCollision(),
        //                 new InterceptTarget(40f, true)
        //             ),
        //             new Sequence(new Node[] {
        //                 new MoveToCalcPosition(
        //                     (ctx)=>{
        //                         var targetTransform = ctx.pilot.NavCalc.TargetBody.transform;
        //                         return targetTransform.position + targetTransform.up * 10f;
        //                     }, 
        //                     (ctx)=>{
        //                         return ctx.pilot.NavCalc.TargetBody.Velocity();
        //                     }
        //                 ),
        //                 new DockAtTarget(),
        //                 new Closure((ctx)=>{
        //                     SetCurrentBehaviour(null); 
        //                     return Node.Result.Success;
        //                 })
        //             })
        //         })
        //     });

        //     SetCurrentBehaviour(goToX);
        // }
    }

    void FixedUpdate() {
        if (Engaged) {
            currentBehaviour.Tick();
        }
    }

    public Node CurrentBehaviour() {
        return currentBehaviour;
    }

    public void SetCurrentBehaviour(Node behaviour) {
        currentBehaviour = behaviour;
    }
}

public class NPCBehaviour {
    public enum Option {
        SearchAndDestroy,
        SurfaceGunner,
        Rest
    }

    public static Node BehaviourFromOption(Option option) {
        switch (option){
            case Option.SearchAndDestroy:
                return SearchAndDestroy();
            case Option.Rest:
                return null;
            case Option.SurfaceGunner:
                return SurfaceGunner();
            default:
                throw new System.NotImplementedException();
        }
    }

    public static Node SearchAndDestroy() {
        return new Selector(new Node[] {
            new Sequence(new Node[] {
                new NavTargetEnemy<TruckController>((match)=>{return !match.immortal;}),
                new Mute(new TakeOff()),
                new Mute(
                    new Parallel(new Node[] {
                        new InterceptTargetAndDodge(80f, false),
                        new Mute(new ShootTarget<TruckController>()),
                    }, true)
                )
            }),
            new While((ctx)=>{return !ctx.Vehicle.Landed;}, 
                new Sequence(new Node[] {
                    new StopWeapons(),
                    new NavTargetClosest<RefuelController>((match)=>{return !match.HasVehicle;}),
                    new InterceptTargetAndDodge(80f, false),
                    new DockAtTarget(),
                })
            ),
            new Wait(5f)
        });
    }

    public static Node SurfaceGunner() {
        return new ShootTarget<UnitController>();
    }
}
