namespace AtaBehaviour {
    using UnityEngine;

    public interface IBehaviourRunner
    {
        Node CurrentBehaviour();
        GameObject gameObject{
            get;
        }
    }
}