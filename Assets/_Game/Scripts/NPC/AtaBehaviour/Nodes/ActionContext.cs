namespace AtaBehaviour {
    using UnityEngine;
    using System;

    public class ActionContext {
        UnitController unit;
        public PhysicsBody physics;

        public UnitController Unit {
            get {
                return unit;
            }
        }

        public VehicleController Vehicle {
            get {
                return NoisyCast<VehicleController>(unit);
            }
        }

        public ActionContext(UnitController unit) {
            this.unit = unit;
            this.physics = unit.GetComponent<PhysicsBody>();
        }

        public double GameTime() {
            return SceneContext.GameTime;
        }

        protected T NoisyCast<T>(object obj) where T: class {
            if (obj == null) {
                Debug.LogWarningFormat("{0} is trying to cast a null obj!", this.GetType().ToString());
                
                return null;
            }

            var cast = obj as T;

            if (cast == null) {
                Debug.LogWarningFormat("{0} is trying to cast {2} on {3} to {4}.  This node might need a different context!", this.GetType(), obj.GetType(), unit.name, typeof(T));
            }

            return cast;
        }
    }
}