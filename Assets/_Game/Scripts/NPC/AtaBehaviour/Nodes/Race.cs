namespace AtaBehaviour {
    // Run in parallel until one succeeds or all fail.
    public class Race: AssyncSequence {
        public Race(Node[] nodes): base(nodes) {
            //
        }

        public override Result Tick()
        {
            bool someRunning = false;
            foreach(NodeTask task in nodeTasks) {
                if (task.Running()) {
                    var result = task.Tick();
                    if (result == Result.Success) {
                        Reset();
                        return LogResult(Result.Success);
                    }

                    if (result == Result.Running) {
                        someRunning = true;
                    }
                }
            }

            if (someRunning) {
                return LogResult(Result.Running);
            } else {
                Reset();

                return LogResult(Result.Failure);
            }
        }
    }
}