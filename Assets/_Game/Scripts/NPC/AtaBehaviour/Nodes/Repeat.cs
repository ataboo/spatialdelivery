namespace AtaBehaviour {
    using UnityEngine;

    public class Repeat : Node
    {
        int repetitions;
        int count;
        Node child;
        int safetyMax = 1000000;

        public Repeat(Node child, int repetitions = -1) {
            count = 0;
            this.child = child;
            this.repetitions = repetitions;
        }

        public override Result Tick()
        {
            int safetyCounter = 0;

            while(repetitions < 0 || count < repetitions) {
                Result result = child.Tick();

                if (repetitions > 0) {
                    count++;
                }

                safetyCounter++;

                if (safetyCounter > safetyMax) {
                    Debug.LogErrorFormat("Infinite loop in behaviours detected: {0}", context.Unit.gameObject.name);
                    Debug.LogError(new BehaviourDebug.LogRow(this).ToStringRecursive());
                    SharedContext.Quit();
                    return LogResult(Result.Failure);
                }

                if (result == Result.Running || result == Result.Success) {
                    return LogResult(Result.Running);
                } else {
                    Reset();
                    return LogResult(result);
                }
            }

            Reset();
            return LogResult(Result.Success);
        }

        public override Node[] Children() {
            return new Node[]{child};
        }

        void Reset() {
            count = 0;
        }
    }
}