namespace AtaBehaviour {
    using UnityEngine;
    using System.Collections.Generic;
    using System.Text;

    public abstract class Node {
        public enum Result {
            Success,
            Failure,
            Running,
            Unset
        }

        protected ActionContext context;

        protected Result loggedResult = Result.Unset;

        public virtual Result LoggedResult() {
            return loggedResult;
        }

        public virtual Node[] Children() {
            return null;
        }

        public virtual void SetContext(ref ActionContext context) {
            this.context = context;

            if (Children() != null) {
                foreach(Node child in Children()) {
                    child.SetContext(ref context);
                }
            }
        }

        public bool HasContext() {
            return this.context != null;
        }

        protected Result LogResult(Result result) {
            loggedResult = result;
            
            return result;
        }

        public abstract Result Tick();
    }
}