using System;

namespace AtaBehaviour {

    public abstract class AssyncSequence : Node
    {
        protected NodeTask[] nodeTasks;
        protected Node[] nodes;

        public AssyncSequence(Node[] nodes) {
            this.nodeTasks = new NodeTask[nodes.Length];
            
            for(int i=0; i<nodes.Length; i++) {
                this.nodeTasks[i] = new NodeTask(nodes[i]);
                this.nodes = nodes;
            }
        }

        protected void Reset() {
            foreach(NodeTask task in nodeTasks) {
                task.Reset();
            }
        }

        public override Node[] Children() {
            return nodes;
        }
    }

    public class NodeTask {
        public Node.Result result;
        public Node node;

        public NodeTask(Node node) {
            this.node = node;
            Reset();
        }

        public void Reset() {
            this.result = Node.Result.Running;
        }

        public Node.Result Tick() {
            this.result = node.Tick();
            return result;
        }

        public bool Running() {
            return result == Node.Result.Running;
        }
    }
}