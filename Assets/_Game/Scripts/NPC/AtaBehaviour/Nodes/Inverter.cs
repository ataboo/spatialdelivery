namespace AtaBehaviour {
    public class Inverter: Node {
        private Node child;
        
        public Inverter(Node child) {
            this.child = child;
        }

        public override Result Tick()
        {
            var result = child.Tick();

            if (result == Result.Success) {
                return LogResult(Result.Failure);
            }

            if (result == Result.Failure) {
                return LogResult(Result.Failure);
            }

            return LogResult(Result.Running);
        }

        public override Node[] Children() {
            return new Node[]{child};
        }
    }
}