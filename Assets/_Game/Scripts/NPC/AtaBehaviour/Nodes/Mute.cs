namespace AtaBehaviour {
    public class Mute : Node
    {
        Node child;

        public Mute(Node child) {
            this.child = child;
        }

        public override Result Tick()
        {
            Result result = child.Tick();

            if (result == Result.Failure) {
                result = Result.Success;
            }

            return LogResult(result);
        }

        public override Node[] Children() {
            return new Node[]{child};
        }
    }
}