using System;

// Run in parallel until all succeed or one fails.
namespace AtaBehaviour {

    public class Parallel : AssyncSequence
    {
        bool repeatSucceeded;

        public Parallel(Node[] nodes, bool repeatSucceeded = false): base(nodes) {
            this.repeatSucceeded = repeatSucceeded;
        }

        public override Result Tick()
        {
            bool someRunning = false;
            foreach(NodeTask task in nodeTasks) {
                if (repeatSucceeded || task.Running()) {
                    var result = task.Tick();
                    if (result == Result.Failure) {
                        Reset();
                        return LogResult(Result.Failure);
                    }

                    if (result == Result.Running) {
                        someRunning = true;
                    }
                }
            }

            if (someRunning) {
                return LogResult(Result.Running);
            } else {
                Reset();

                return LogResult(Result.Success);
            }
        }
    }
}