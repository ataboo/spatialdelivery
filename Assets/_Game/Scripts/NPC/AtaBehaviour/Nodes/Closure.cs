namespace AtaBehaviour {
    public class Closure : Node
    {
        ClosureAction action;

        public override Node[] Children() {
            return null;
        }

        public Closure(ClosureAction action) {
            this.action = action;
        }

        public override Result Tick()
        {
            return LogResult(action(context));
        }

        public delegate Result ClosureAction(ActionContext context);
    }
}