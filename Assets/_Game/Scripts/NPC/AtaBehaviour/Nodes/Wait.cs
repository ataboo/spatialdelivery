namespace AtaBehaviour {
    public class Wait: Node {
        double endTime;
        float length;
        
        public Wait(float length){
            this.length = length;
        }

        public override Result Tick() {
            if (endTime < 0) {
                endTime = context.GameTime() + length;
            }

            if (context.GameTime() > endTime) {
                endTime = -1;
                return LogResult(Result.Success);
            }

            return LogResult(Result.Running);
        }
    }
}