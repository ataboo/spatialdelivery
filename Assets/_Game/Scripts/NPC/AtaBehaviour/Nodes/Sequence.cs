
namespace AtaBehaviour {
    public class Sequence: Node {
        protected Node[] nodes;
        int step = 0;

        public Sequence(Node[] nodes) {
            this.nodes = nodes;
        }
        public Sequence() {
            //
        }

        public override Result Tick()
        {
            while(step < nodes.Length) {
                var current = nodes[step];
                var result = current.Tick();

                if (result == Result.Failure) {
                    Reset();
                    return LogResult(Result.Failure);
                }

                if (result == Result.Running) {
                    return LogResult(Result.Running);
                }

                step++;
            }

            Reset();

            return LogResult(Result.Success);
        }

        public override Node[] Children() {
            return nodes;
        }

        void Reset() {
            step = 0;
        }
    }
}