namespace AtaBehaviour {
    public class While: Node {
        Condition condition;
        Node child;

        public While(Condition condition, Node child) {
            this.condition = condition;
            this.child = child;
        }
        
        public override Result Tick() {
            if (!condition(context)) {
                return LogResult(Result.Failure);
            }

            return LogResult(child.Tick());
        }

        public override Node[] Children() {
            return new Node[]{child};
        }

        public delegate bool Condition(ActionContext context);
    }
}