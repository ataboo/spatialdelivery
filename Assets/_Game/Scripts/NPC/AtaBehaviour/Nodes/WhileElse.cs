namespace AtaBehaviour {
    public class WhileElse: Node {
        protected Node whileNode;
        protected Node elseNode;
        protected While.Condition condition;

        public WhileElse() {
            //
        }

        public WhileElse(While.Condition condition, Node whileNode, Node elseNode) {
            this.condition = condition;
            this.whileNode = whileNode;
            this.elseNode = elseNode;
        }

        public override Result Tick()
        {
            if (condition(context)) {
                return LogResult(whileNode.Tick());
            } else {
                return LogResult(elseNode.Tick());
            }
        }

        public override Node[] Children() {
            return new Node[]{whileNode, elseNode};
        }
    }
}