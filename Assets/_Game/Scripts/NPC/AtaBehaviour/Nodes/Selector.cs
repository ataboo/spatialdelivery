namespace AtaBehaviour {
    public class Selector : Node
    {
        protected Node[] nodes;
        int step;

        public Selector(Node[] nodes) {
            this.nodes = nodes;
            step = 0;
        }

        public override Result Tick() {
            while(step < nodes.Length) {
                var current = nodes[step];
                var result = current.Tick();

                if (result == Result.Running) {
                    return LogResult(Result.Running);
                }

                if (result == Result.Success) {
                    step = 0;
                    return LogResult(Result.Success);
                }
                
                step++;
            }

            step = 0;
            return LogResult(Result.Failure);
        }

        public override Node[] Children() {
            return nodes;
        }
    }
}