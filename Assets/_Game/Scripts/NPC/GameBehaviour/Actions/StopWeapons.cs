namespace GameBehaviour {
    using AtaBehaviour;

    public class StopWeapons: Node {
        public override Result Tick() {
            context.Unit.NpcWeapons.StopTracking();
            context.Unit.NpcWeapons.StopFiring();
            return Result.Success;
        }
    }
}