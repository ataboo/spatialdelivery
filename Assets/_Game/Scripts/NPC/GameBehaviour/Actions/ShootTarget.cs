namespace GameBehaviour {
    using AtaBehaviour;
    using UnityEngine;
    using System;
    
    public class ShootTarget<T>: Node where T:UnitController {
        public override Result Tick() {
            var weapons = context.Unit.NpcWeapons;
            
            if (weapons.TrackClosestTarget<T>()) {
                weapons.SetActive(true);
                weapons.FireWithLOS();

                return LogResult(Result.Running);
            }

            weapons.StopFiring();

            return LogResult(Result.Failure);
        }
    }
}