namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class DockAtTarget: Node {
        public DockAtTarget() {
            //
        }

        public override Result Tick() {
            var navCalc = context.Vehicle.Pilot.NavCalc; 
            PhysicsBody targetBody = navCalc.TargetBody;
            
            if(targetBody == null) {
                return LogResult(Result.Failure);
            }

            if (context.Vehicle.CanLockLanding()) {
                context.Vehicle.ToggleLandingLock();
            
                return LogResult(Result.Success);
            }

            var finalAngle = Mathf.LerpAngle(context.Unit.transform.rotation.eulerAngles.z, targetBody.transform.rotation.eulerAngles.z, Time.fixedDeltaTime);
            context.Unit.transform.rotation = Quaternion.Euler(0, 0, finalAngle);
            
            Vector2 landPos = (Vector2)targetBody.transform.position;
            Vector2 deltaPos = landPos - (Vector2)context.Unit.transform.position;

            NavigationCalc.NavSolution solution;
            if (deltaPos.magnitude > 16.5f) {
                solution = navCalc.CrawlPastPos(landPos + (Vector2)targetBody.transform.up * 16f, 6f, targetBody.Velocity());
            } else {
                solution = navCalc.CrawlPastTarget(1);
            }
            if (solution.result == Node.Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            }

            return solution.result;
        }
    }
}