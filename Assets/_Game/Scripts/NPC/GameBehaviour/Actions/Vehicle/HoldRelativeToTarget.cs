namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class HoldRelativeToTarget : Node
    {
        TargetOffsetDelegate offsetDelegate;

        public HoldRelativeToTarget(TargetOffsetDelegate offsetDelegate, float maxVelocity) {
            this.offsetDelegate = offsetDelegate;
        }

        public override Result Tick()
        {   
            var navCalc = context.Vehicle.Pilot.NavCalc;
            var target = navCalc.TargetBody;

            if (target == null) {
                return LogResult(Result.Failure);
            }

            Vector2 targetPos = offsetDelegate(target);

            var solution = navCalc.CrawlPastPos(targetPos);
            if (solution.result == Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            }

            return LogResult(solution.result);
        }

        public delegate Vector2 TargetOffsetDelegate(PhysicsBody target);
    }
}