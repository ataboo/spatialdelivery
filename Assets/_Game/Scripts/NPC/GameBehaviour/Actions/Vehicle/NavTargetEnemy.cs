namespace GameBehaviour {
    using UnityEngine;

    public class NavTargetEnemy<T>: NavTargetClosest<T> where T: UnitController {
        public NavTargetEnemy(TypeFilterDelegate typeFilter = null) {
            this.typeFilter = (T match) => {
                if(match.Faction.AlliedTo(context.Vehicle.Faction)) {
                    return false;
                }

                if (typeFilter != null) {
                    return typeFilter(match);
                }

                return true;
            };
        }
    }
}