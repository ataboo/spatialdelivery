namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class InterceptTargetAndDodge: WhileElse {
        public InterceptTargetAndDodge(float standoff, bool stopAtTarget): base() {
            this.condition = (ctx)=>{return ctx.Vehicle.Collision.NextCollision != null;};
            this.whileNode = new DodgeCollision();
            this.elseNode = new InterceptTarget(standoff, stopAtTarget);
        }
    }
}