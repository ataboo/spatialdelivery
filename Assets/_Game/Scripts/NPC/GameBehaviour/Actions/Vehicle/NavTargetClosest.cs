namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;
    using System;

    public class NavTargetClosest<T>: Node where T: Component
    {
        protected TypeFilterDelegate typeFilter;

        public NavTargetClosest(TypeFilterDelegate typeFilter = null) {
            this.typeFilter = typeFilter;
        }

        public override Result Tick()
        {
            var matches = GameObject.FindObjectsOfType(typeof(T));
            if (matches.Length == 0) {
                return LogResult(Result.Failure);
            }

            float closestMagSqr = float.MaxValue;
            T closestMatch = null;
            foreach(T match in matches) {
                if (match.transform.root.gameObject == context.Unit.gameObject) {
                    continue;
                }

                if (typeFilter != null) {
                    if (!typeFilter(match)) {
                        continue;
                    }
                }
                
                float rangeSqr = (match.transform.position - context.Unit.transform.position).sqrMagnitude;
                if(rangeSqr < closestMagSqr) {
                    closestMagSqr = rangeSqr;
                    closestMatch = match;
                }
            }

            if (closestMatch == null) {
                return LogResult(Result.Failure);
            }

            context.Vehicle.Pilot.NavCalc.SetNavTarget(closestMatch.GetComponent<PhysicsBody>());

            return LogResult(Result.Success);
        }

        public delegate bool TypeFilterDelegate(T match);
    }
}