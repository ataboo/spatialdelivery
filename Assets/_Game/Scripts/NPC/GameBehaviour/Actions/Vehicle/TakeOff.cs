namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class TakeOff: Sequence
    {
        public TakeOff() {
            // var moveNode = new MoveToPosition(Vector2.zero, false);
            nodes = new Node[] {
                new Closure((ctx)=>{
                    if (!ctx.Vehicle.landingJoint.enabled) {
                        return Result.Failure;
                    }
                    ctx.Vehicle.ToggleLandingLock();

                    return Result.Success;
                }),
                new MoveUp(10f)
            };
        }
    }
}