namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class DodgeCollision: Node {
        public DodgeCollision() {
        }

        public override Result Tick() {
            return LogResult(DodgeNextCollision());
        }

        Result DodgeNextCollision() {
            var contact = context.Vehicle.Collision.NextCollision;
            if (contact == null) {
                return Result.Failure;
            }

            var solution = context.Vehicle.Pilot.NavCalc.DodgeCollision(contact);
            context.Vehicle.InputUpdate(solution.input);

            return solution.result;
        }
    }
}