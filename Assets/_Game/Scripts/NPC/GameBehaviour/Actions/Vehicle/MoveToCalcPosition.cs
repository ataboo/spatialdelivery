namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;
    using System;

    public class MoveToCalcPosition: Node {
        bool stopAtPos;
        VectorClosure positionDelegate;
        VectorClosure velocityDelegate;
        
        public MoveToCalcPosition(VectorClosure positionDelegate, VectorClosure velocityDelegate) {
            this.positionDelegate = positionDelegate;
            this.velocityDelegate = velocityDelegate;
        }

        public override Result Tick() {
            NavigationCalc navCalc = context.Vehicle.Pilot.NavCalc;
            NavigationCalc.NavSolution solution;

            var targetPos = positionDelegate(context);
            var targetVelocity = velocityDelegate(context);
            stopAtPos = targetVelocity != null;
            if (stopAtPos) {
                solution = navCalc.StopAtPosition(targetPos, targetVelocity);
            } else {
                solution = navCalc.MovePastPosition(targetPos);
            }

            if (solution.result == Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            }

            return LogResult(solution.result);
        }

        public delegate Vector2 VectorClosure (ActionContext ctx);
    }
}