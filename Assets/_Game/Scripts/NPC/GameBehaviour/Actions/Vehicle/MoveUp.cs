namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class MoveUp: Node {
        float distance;
        Vector2 targetPos;
        bool startedMoving;
        
        public MoveUp(float distance) {
            this.distance = distance;
            Reset();
        }

        void Reset() {
            startedMoving = false;
        }

        public override Result Tick() {
            if (!startedMoving) {
                targetPos = context.Unit.transform.position + context.Unit.transform.up * distance;
                startedMoving = true;
            }

            var solution = context.Vehicle.Pilot.NavCalc.MovePastPosition(targetPos);

            if (solution.result == Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            } else {
                Reset();
            }

            return solution.result;
        }
    }
}