namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;
    using System;

    public class MoveToPosition: Node {
        bool stopAtPos;
        Vector2 finalVelocity;
        Vector2 targetPos;
        
        public MoveToPosition(Vector2 targetPos, Nullable<Vector2> finalVelocity) {
            this.targetPos = targetPos;

            this.stopAtPos = finalVelocity != null;
            if (stopAtPos) {
                this.finalVelocity = finalVelocity.Value;
            }
        }

        public override Result Tick() {
            var navCalc = context.Vehicle.Pilot.NavCalc;
            NavigationCalc.NavSolution solution;
            if (stopAtPos) {
                solution = navCalc.StopAtPosition(targetPos, finalVelocity);
            } else {
                solution = navCalc.MovePastPosition(targetPos);
            }

            if (solution.result == Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            }

            return LogResult(solution.result);
        }
    }
}