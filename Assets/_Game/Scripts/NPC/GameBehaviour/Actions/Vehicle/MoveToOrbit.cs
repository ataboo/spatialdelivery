namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;
    using System;

    public class MoveToOrbit: Node {
        CircularOrbit orbit;
    

        public MoveToOrbit(CircularOrbit orbit) {
            this.orbit = orbit;
        }

        public override Result Tick() {
            return LogResult(TickToOrbit());
        }

        Result TickToOrbit() {
            if (orbit.focalPoint == null) {
                orbit.focalPoint = context.Vehicle.SatBody.OrbitCenter() as RailsBody;
            }

            var solution = context.Vehicle.Pilot.NavCalc.MoveToOrbit(orbit);
            context.Vehicle.InputUpdate(solution.input);

            var newRotation = Vector2.Lerp(context.Vehicle.transform.up, (context.Vehicle.transform.position - orbit.focalPoint.transform.position).normalized, Time.fixedDeltaTime);
            context.Unit.transform.rotation = Quaternion.FromToRotation(Vector2.up, newRotation);

            return solution.result;
        }
    }
}