namespace GameBehaviour {
    using UnityEngine;
    using AtaBehaviour;

    public class InterceptTarget: Node {
        float standoffRange;
        bool stopAtTarget;

        public InterceptTarget(float standoffRange, bool stopAtTarget = false): base() {
            this.standoffRange = standoffRange;
            this.stopAtTarget = stopAtTarget;
        }

        public override Result Tick() {
            return LogResult(MoveToTarget());
        }

        Result MoveToTarget() {
            var solution = context.Vehicle.Pilot.NavCalc.MoveToTarget(stopAtTarget, standoffRange);
            if (solution.result == Result.Running) {
                context.Vehicle.InputUpdate(solution.input);
            }

            return solution.result;
        }
    }
}