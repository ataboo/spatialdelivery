namespace AtaBehaviour {
    using UnityEngine;
    using System;

    public class NavigationCalc {
        float maxVelocity;
        float haste;
        public SuccessMargin closeEnoughStop = new SuccessMargin(2f, 0.5f);
        public SuccessMargin closeEnoughVia = new SuccessMargin(6f, 1f);
        public SuccessMargin closeEnoughCrawl = new SuccessMargin(0.5f, 0.1f);

        PhysicsBody targetBody;
        
        ActionContext context;

        public float MaxVelocity {
            get {
                return maxVelocity;
            }
        }

        public PhysicsBody TargetBody {
            get {
                return targetBody;
            }
        }

        public NavigationCalc(ActionContext context, float maxVelocity = 20f, float haste = 0.05f) {
            this.context = context;
            this.maxVelocity = maxVelocity;
            this.haste = haste;
        }

        public NavSolution MovePastPosition(Vector2 targetPos) {
            Vector2 currentPos = context.Unit.transform.position;
            var targetVelocity = VelocityToPosition(targetPos, null);

            if ((targetPos - currentPos).magnitude < closeEnoughVia.distance) {
                return new NavSolution(Node.Result.Success, Vector2.zero);
            }

            var input = InputForVelocity(targetVelocity, targetPos, false, closeEnoughVia);

            return new NavSolution(Node.Result.Running, input);
        }

        public NavSolution StopAtPosition(Vector2 targetPos, Vector2 finalVelocity) {
            Vector2 currentPos = context.Unit.transform.position;
            var targetVelocity = VelocityToPosition(targetPos, finalVelocity);
            var deltaPos = targetPos - currentPos;
            var deltaV = finalVelocity - context.physics.Velocity();

            if (deltaPos.magnitude < closeEnoughStop.distance && deltaV.magnitude < closeEnoughStop.velocity) {
                return new NavSolution(Node.Result.Success, Vector2.zero);
            }

            var input = InputForVelocity(targetVelocity, targetPos, true, closeEnoughStop);

            return new NavSolution(Node.Result.Running, input);
        }

        public NavSolution MoveToTarget(bool stopAtPos, float standoffRange = 20f) {
            if (targetBody == null) {
                return new NavSolution(Node.Result.Failure, Vector2.zero);
            }

            Vector2 currentPos = context.Unit.transform.position;
            Vector2 deltaTarget = (Vector2)targetBody.transform.position - currentPos;
            float ete = ETE(Mathf.Min(deltaTarget.magnitude - standoffRange, 0f));
            Vector2 predictedTargetPos = (Vector2)targetBody.transform.position + targetBody.Velocity() * ete;
            var targetPos = predictedTargetPos - (predictedTargetPos - currentPos).normalized * standoffRange;

            Debug.DrawLine(currentPos, predictedTargetPos, Color.cyan);

            if (stopAtPos) {
                return StopAtPosition(targetPos, targetBody.Velocity());
            } else {
                return MovePastPosition(targetPos);
            }
        }

        public NavSolution MoveToOrbit(CircularOrbit orbit) {
            Vector2 vehiclePos = context.Unit.transform.position;
            Vector2 deltaPos = orbit.focalPoint.transform.position - context.Unit.transform.position;
            float radius = deltaPos.magnitude;
            float radiusError = radius - orbit.semiMajor;
            float lateralScalar = orbit.clockwise ? orbit.VelocityMagnitude() : -orbit.VelocityMagnitude();

            Vector2 lateralVelocity = lateralScalar * Vector2.Perpendicular(deltaPos).normalized;
            Vector2 verticalVelocity = deltaPos.normalized * Mathf.Clamp(radiusError, -maxVelocity/4, maxVelocity/4);

            Vector2 targetVelocity = lateralVelocity + verticalVelocity;
            Vector2 deltaV = targetVelocity - context.physics.Velocity();

            Debug.DrawLine(vehiclePos, vehiclePos+deltaPos, Color.magenta);

            var closeEnough = new SuccessMargin(0.01f, 0.01f);

            if(Mathf.Abs(radiusError) < closeEnough.distance && deltaV.magnitude < closeEnough.velocity) {
                return new NavSolution(Node.Result.Success, Vector2.zero);
            }

            var input = InputForVelocity(lateralVelocity + verticalVelocity, null, false, closeEnough);
            return new NavSolution(Node.Result.Running, input);
        }

        public NavSolution CrawlPastTarget(float velocity = 1f) {
            if (targetBody == null) {
                return new NavSolution(Node.Result.Failure, Vector2.zero);
            }

            return CrawlPastPos((Vector2)targetBody.transform.position, velocity, targetBody.Velocity());
        }

        public NavSolution CrawlPastPos(Vector2 position, float approachVelocity = 1f, Nullable<Vector2> finalVelocity = null) {
            if (finalVelocity == null) {
                finalVelocity = Vector2.zero;
            }

            Vector2 targetVelocity = (position - (Vector2)context.Unit.transform.position).normalized * approachVelocity + finalVelocity.Value;
            var input = InputForVelocity(targetVelocity, null, false, closeEnoughCrawl);
            
            return new NavSolution(Node.Result.Running, input);
        }

        float ETE(float deltaTarget) {
            return deltaTarget / MaxVelocity;
        }

        public NavSolution DodgeCollision(CollisionDetector.CollisionContact contact) {
            if (TargetBody == null) {
                return new NavSolution(Node.Result.Failure, Vector2.zero);
            }

            Vector2 currentPos = context.Unit.transform.position;
            // right +ve
            Vector2 deltaContact = ((Vector2)contact.body.transform.position - currentPos);
            Vector2 contactLateralAxis = Vector2.Perpendicular(deltaContact).normalized;
            float lateralVelocity;
            Vector2 deltaPos = TargetBody.transform.position - context.Unit.transform.position;

            var targetLateralProjection = AtaMath.Projections(deltaPos, contactLateralAxis);

            if (targetLateralProjection.scalar > 0) {
                lateralVelocity = maxVelocity; 
            } else {
                lateralVelocity = -maxVelocity;
            }
            var targetVelocity = contactLateralAxis * lateralVelocity + deltaContact.normalized * -maxVelocity/4;

            var input = InputForVelocity(targetVelocity, null, false, closeEnoughVia);

            return new NavSolution(Node.Result.Running, input);
        }

        public void SetNavTarget(PhysicsBody targetBody) {
            this.targetBody = targetBody;
        }

        Vector2 VelocityToPosition(Vector2 targetPos, Nullable<Vector2> finalVelocity) {
            bool stopAtPos = finalVelocity != null;
            Vector2 currentPos = context.Unit.transform.position;
            var toTarget = targetPos - currentPos;
      
            if (stopAtPos) {
                var closureProjection = AtaMath.Projections(context.physics.Rigidbody().velocity, toTarget);
                float stopAccel = (closureProjection.scalar / (2 * toTarget.magnitude)) / MaxAcceleration();
                
                float targetClosure = maxVelocity;
                if (stopAccel > haste) {
                    targetClosure *= (haste - stopAccel);
                }

                return toTarget.normalized * targetClosure + finalVelocity.Value;
            }

            return Vector2.ClampMagnitude(toTarget.normalized * maxVelocity, maxVelocity);
        }

        public Vector2 InputForVelocity(Vector2 goalVelocity, Nullable<Vector2> targetPos, bool stopAtPos, SuccessMargin margin) {
            Vector2 deltaV = goalVelocity - context.physics.Rigidbody().velocity;
            Vector2 input = Vector2.zero;

            if (deltaV.magnitude > margin.velocity) {
                Vector2 requiredAcceleration = deltaV * Mathf.Pow(deltaV.magnitude, 4);
                
                Vector2 accelerationScaled = requiredAcceleration / MaxAcceleration();
                accelerationScaled = Vector2.ClampMagnitude(accelerationScaled, 1f);


                if (stopAtPos) {
                    Vector2 toTarget = targetPos.Value - (Vector2)context.Unit.transform.position;
                    var accelCurved = accelerationScaled * context.Vehicle.Pilot.hasteCurve.Evaluate(toTarget.magnitude / 100f);
                    input = Quaternion.Euler(0, 0, -context.physics.Rigidbody().rotation) * accelCurved;
                } else {
                    input = Quaternion.Euler(0, 0, -context.physics.Rigidbody().rotation) * accelerationScaled;
                }
            }

            return input;
        }

        float MaxAcceleration() {
            return context.Vehicle.thrustForce / context.physics.Rigidbody().mass;
        }

        public struct NavSolution {
            public Node.Result result;
            public Vector2 input;

            public NavSolution(Node.Result result, Vector2 input) {
                this.result = result;
                this.input = input;
            }
        }

        public struct SuccessMargin {
            public float distance;
            public float velocity;
        
            public SuccessMargin(float distance, float velocity) {
                this.distance = distance;
                this.velocity = velocity;
            }
        }

    }
}