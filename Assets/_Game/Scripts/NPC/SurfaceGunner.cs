using UnityEngine;
using AtaBehaviour;
using GameBehaviour;

public class SurfaceGunner: MonoBehaviour, IBehaviourRunner {
    public NPCBehaviour.Option behavior;

    Node currentBehaviour;
    UnitController unit;
    ActionContext context;


    void Awake() {
        unit = GetComponent<UnitController>();
        context = new ActionContext(unit);
        currentBehaviour = NPCBehaviour.BehaviourFromOption(behavior);
        if (currentBehaviour != null) {
           currentBehaviour.SetContext(ref context);
        }
    }

    Node IBehaviourRunner.CurrentBehaviour() {
        return currentBehaviour;
    }

    void FixedUpdate() {
        if (currentBehaviour != null) {
            currentBehaviour.Tick();
        }
    }
}