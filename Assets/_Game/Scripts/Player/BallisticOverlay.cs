// using UnityEngine;
// using System.Collections.Generic;

// public class BallisticOverlay: MonoBehaviour {
//     public bool showBallistic = true;
//     private Vector2[] projections;
//     private Rigidbody2D rb;
//     private OrbitalOverlay orbitalOverlay;
//     private LayerMask orbitalLayer;
//     public GravitationalController gravitationalController;

//     void Start() {
//         orbitalOverlay = GetComponent<OrbitalOverlay>();
//         orbitalLayer = LayerMask.GetMask("OrbitalBody");
//         rb = GetComponent<Rigidbody2D>();
//     }

//     void FixedUpdate() {
//         projections = ProjectPosition();
//     }

//     void OnDrawGizmosSelected() {
//         if (!showBallistic) {
//             return;
//         }

//         if (rb != null) {
//             Gizmos.color = Color.blue;
//             Gizmos.DrawLine(transform.position, projections[0]);
//             for(int i=0; i<projections.Length-1; i++) {
//                 Gizmos.DrawLine(projections[i], projections[i+1]);
//             }
//         }
//     }

//     Vector2[] ProjectPosition() {
//         var projector = new BallisticProjector(0.02f, 250);
//         var projections = projector.Project(rb, gravitationalController.GetOrbitalBodies()).ToArray();

//         if (orbitalOverlay != null) {
//             orbitalOverlay.parentBody = projector.currentParentBody;
//         }

//         return projections;
//     }
// }

// class BallisticProjector {
//     float deltaPole;
//     Vector2 lastPos;
//     Vector2 lastV;
//     int poleCount;
//     List<Vector2> projections;
//     List<OrbitalBodyContact> contacts;
//     public OrbitalBody currentParentBody;

//     public BallisticProjector(float deltaPole, int poleCount) {
//         projections = new List<Vector2>(poleCount);
//         this.deltaPole = deltaPole;
//         this.poleCount = poleCount;
//     }

//     public List<Vector2> Project(Rigidbody2D satellite, List<OrbitalBodyContact> contacts) {
//         projections.Clear();
//         this.lastPos = satellite.gameObject.transform.position;
//         this.lastV = satellite.velocity;
//         this.contacts = contacts;
//         float time = Time.time;

//         for(int i=0; i<this.poleCount; i++) {
//             time += this.deltaPole;
//             var (newPos, parentBody) = ProjectedPositionAtTime(time);
//             projections.Add(newPos);

//             if (i==0) {
//                 this.currentParentBody = parentBody;
//             }
//         }

//         return projections;
//     }

//     (Vector2, OrbitalBody) ProjectedPositionAtTime(float time) {
//         var netAccel = Vector2.zero;
//         OrbitalBody closestBody = null;
//         Vector2 closestBodyProjectedPos = Vector2.zero;
//         float closestRange = 0f;
        
//         foreach(OrbitalBodyContact contact in contacts) {
//             var contactStats = contact.StatsAtTime(lastPos, time);

//             float range = contactStats.bearing.magnitude - contact.body.approxRadius;

//             if (range < closestRange || closestBody == null) {
//                 closestBody = contact.body;
//                 closestRange = range;
//                 closestBodyProjectedPos = contactStats.position;
//             }

//             netAccel += contactStats.accel;
//         }

//         Vector2 newPos = lastPos + lastV * deltaPole + (netAccel * deltaPole * deltaPole) / 2;
//         Vector2 newV = lastV + netAccel * deltaPole;

//         this.lastV = newV;
//         this.lastPos = newPos;

//         if (closestBody != null) {
//             var bodyOffset = closestBodyProjectedPos - (Vector2)closestBody.transform.position;
//             newPos -= bodyOffset;
//         }

//         return (newPos, closestBody);
//     }
// }