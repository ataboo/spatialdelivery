﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBehaviour;

public class PlayerController : MonoBehaviour
{
    public VehicleController currentVehicle;
    public CameraController camControl;
    public WeaponBarController weaponBar;
    bool showingRestartToast = false;
    Vector3 flightInput;

    void Start() {
        camControl = SceneContext.instance.MainCamController();
        camControl.target = currentVehicle.transform;
        weaponBar = SceneContext.instance.WeaponBarController();

        if (currentVehicle != null) {
            SetCurrentVehicle(currentVehicle);
        }
    }

    void FixedUpdate() {
        if (currentVehicle == null) {
            return;
        }

        if (currentVehicle.Pilot == null || !currentVehicle.Pilot.Engaged) {
            currentVehicle.InputUpdate(flightInput);
        }
    }

    void Update() {
        if (SharedContext.instance.IsPaused()) {
            return;
        }

        if (currentVehicle == null) {
            if (!showingRestartToast) {
                SharedContext.ShowToast("Hit [Esc] to exit or restart", 0);
                showingRestartToast = true;
            }
            return;
        }

        flightInput = new Vector3(Input.GetAxis(AtaInput.HORIZ), Input.GetAxis(AtaInput.VERT), Input.GetAxis(AtaInput.ROTATE));

        if (showingRestartToast) {
            SharedContext.ClearToasts();
            showingRestartToast = false;
        }

        if (Input.GetButtonDown(AtaInput.TIME_UP)) {
            SharedContext.instance.IncreaseTimeScale();
        } 
        if (Input.GetButtonDown(AtaInput.TIME_DOWN)) {
            SharedContext.instance.DecreaseTimeScale();
        }

        if (currentVehicle.Pilot == null || !currentVehicle.Pilot.Engaged) {
            UpdateVehicleInputs();
            UpdateWeaponInputs();
        }
    }

    void UpdateVehicleInputs() {
        if (Input.GetKeyDown(KeyCode.X)) {
            currentVehicle.Pilot.SetCurrentBehaviour(new MoveToOrbit(new CircularOrbit(250, 0, null, false)));
        }
        if (Input.GetKeyDown(KeyCode.C)) {
            currentVehicle.Pilot.SetCurrentBehaviour(null);
        }
        
        if (Input.GetButtonDown(AtaInput.FORK_GRAB)) {
            currentVehicle.TogglePayload();
        }

        if (Input.GetButtonDown(AtaInput.LAND)) {
            currentVehicle.ToggleLandingLock();
        }

        if (Input.GetButtonDown(AtaInput.GEAR)) {
            currentVehicle.ToggleLandingGear();
        }

        if (Input.GetButtonDown(AtaInput.SWAP_VEHICLE)) {
            
            var nextVehicle = currentVehicle.NextShipAtSpaceport();
            // TODO permission to fly  the vehicle.
            if (nextVehicle != null) {
                if (currentVehicle != null && currentVehicle.NpcWeapons != null) {
                    currentVehicle.SwitchToNPCWeapons();
                }

                SetCurrentVehicle(nextVehicle);
            }
        }
    }

    void UpdateWeaponInputs() {
        if (currentVehicle.PlayerWeapons == null) {
            return;
        }
 
        if (Input.GetButtonDown(AtaInput.WEAPON_1)) {
            currentVehicle.PlayerWeapons.ToggleActive();
            weaponBar.SetTurretState(currentVehicle.PlayerWeapons.WeaponState);
        }

        if (Input.GetButtonDown(AtaInput.FIRE)) {
            currentVehicle.PlayerWeapons.Fire();
        }

        if (Input.GetButtonUp(AtaInput.FIRE)) {
            currentVehicle.PlayerWeapons.StopFiring();
        }

        if (Input.GetButtonUp(AtaInput.LOCK)) {
            currentVehicle.PlayerWeapons.TargetClosestVehicle();
            weaponBar.SetTurretState(currentVehicle.PlayerWeapons.WeaponState);
        }
    }

    public float HealthFraction() {
        if (currentVehicle == null) {
            return 0f;
        }
        
        return currentVehicle.HealthFraction();
    }

    public float FuelFraction() {
        if (currentVehicle == null) {
            return 0f;
        }

        return currentVehicle.FuelFraction();
    }

    public float TimeFraction() {
        return SharedContext.instance.TimeCompressionFraction();
    }

    void SetCurrentVehicle(VehicleController vehicle) {
        currentVehicle = vehicle;
        camControl.target = vehicle.transform;

        var weaponState = WeaponController.State.Disabled;

        if (currentVehicle.PlayerWeapons != null) {
            currentVehicle.SwitchToPlayerWeapons();
            weaponState = currentVehicle.PlayerWeapons.WeaponState;
        }

        weaponBar.SetTurretState(weaponState);
    }
}

public interface LandingEventDelegate {
    void OnLandedAtObject(GameObject baseObject);
    void OnTakeoffFromObject(GameObject baseObject);
}