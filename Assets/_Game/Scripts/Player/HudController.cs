﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudController : MonoBehaviour
{
    public Image healthBar;
    public Image fuelBar;
    public Image timeBar;

    void Update() {
        var player = SceneContext.instance.Player();
        
        if (player != null) {
            healthBar.fillAmount = player.HealthFraction();
            fuelBar.fillAmount = player.FuelFraction();
            timeBar.fillAmount = player.TimeFraction();
        } else {
            healthBar.fillAmount = 0;
            fuelBar.fillAmount = 0;
        }
    }
}
