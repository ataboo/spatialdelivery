﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Vectrosity;

// Use current velocity and position to calculate orbital values.  Draw the calculated orbit trajectory.
public class OrbitalOverlay : MonoBehaviour, MapLine
{
    public float eccentricity;
    public float eccentricAnomoly;
    public float meanAnomoly;
    public float semiMajor;
    private Rigidbody2D rb;
    private VehicleController vehicle;

    public int projectedPointCount = 100;
    private VectorLine orbitalLine;
    private SatelliteBody satelliteBody;

    public float lineZ = 1f;

    void Awake() {
        rb = GetComponent<Rigidbody2D>();
        vehicle = GetComponent<VehicleController>();
        satelliteBody = GetComponent<SatelliteBody>();

        orbitalLine = new VectorLine("orbital-overlay_"+name, new List<Vector3>(projectedPointCount), 2f);
        orbitalLine.color = Color.green;
        orbitalLine.lineType = LineType.Continuous;
        orbitalLine.joins = Joins.Weld;
        orbitalLine.Draw3DAuto();

        var renderHider = GetComponent<RenderHider>();
        if (renderHider != null) {
            renderHider.mapLine = this;
        }
    }

    void LateUpdate() {
        if (satelliteBody.OrbitCenter() != null) {
            CalcEccentricity();
            UpdateLine();
        }
    }

    void UpdateLine() {
        var points = orbitalLine.points3;
        UpdateConicPoints(ref points);
        orbitalLine.points3 = points;
    }

    public void ShowLine() {
        orbitalLine.active = true;
    }

    public void HideLine() {
        orbitalLine.active = false;
    }

    public bool LineIsVisible()
    {
        return orbitalLine.active;
    }

    void OnDestroy() {
        VectorLine.Destroy(ref orbitalLine);
    }

    private void CalcEccentricity() {
        NewtonianBody parentBody = satelliteBody.OrbitCenter();

        // Just throw a salute to Keplar, plug in values, and walk away.
        float mu = parentBody.Mass() * SharedContext.GRAV_CONST;
        Vector3 r = transform.position - parentBody.transform.position;
        float radMag = r.magnitude;
        Vector2 velocity = rb.velocity - parentBody.Velocity();
        float vMag = velocity.magnitude;
        Vector3 angular =  Vector3.Cross(r, velocity);

        Vector2 E = Vector3.Cross(velocity, angular) / mu - r / radMag;
        eccentricity = E.magnitude;
        semiMajor = angular.z * angular.z / (mu * (1 - eccentricity * eccentricity));

        eccentricAnomoly = Mathf.Atan2(E.y, E.x);
        
        float periapsis = semiMajor - (semiMajor * eccentricity);
        float apoapsis = semiMajor + (semiMajor * eccentricity);
        float period = 2 * Mathf.PI * Mathf.Sqrt(Mathf.Pow(semiMajor, 3)/mu);

        // Debug.LogFormat("Angular: {0}, Mu: {1} SemiMajor: {2}, Apo: {3}, Peri: {4}, eccentricAnom: {5}, meanAnom: {6}", angular, mu, semiMajor, apoapsis, periapsis, eccentricAnomoly, meanAnomoly);
    }

    private void UpdateConicPoints(ref List<Vector3> points) {
        var (startAngle, endAngle) = ConicStartAndEnd();

        for (int i=0; i<projectedPointCount; i++) {
            float t = (float)(i)/(float)(projectedPointCount - 1);
            float anomaly;
            if (eccentricity < 1) {
                anomaly = SlopedLerp(startAngle, endAngle, t, 0.16f * eccentricity);
            } else {
                anomaly = Mathf.Lerp(startAngle, endAngle, t);
            }

            float rad = semiMajor * (1f - eccentricity  * eccentricity) / (1f + eccentricity * Mathf.Cos(anomaly));
            Vector3 pos = Quaternion.Euler(0, 0, (anomaly + eccentricAnomoly) * Mathf.Rad2Deg) * new Vector3(rad, 0, lineZ);
            pos += satelliteBody.OrbitCenter().transform.position;

            points[i] = pos;
        }
    }

    private (float startAngle, float endAngle) ConicStartAndEnd() {
        float startAngle;
        float endAngle;

        if (eccentricity >= 1) {
            //Hyperbolic trajectory
            startAngle = -Mathf.Acos(-1f/eccentricity) + 0.01f;
            endAngle = -startAngle;
        } else {
            //Elliptical trajectory
            startAngle = 0;
            endAngle = Mathf.PI * 2;
        }

        return (startAngle, endAngle);
    }

    private float SlopedLerp(float min, float max, float t, float sinWeight) {
        float sinT = Mathf.Sin(t * Mathf.PI * 2f);
        float trueT = sinT * sinWeight + t;

        return trueT * (max - min) + min;
    }
}
