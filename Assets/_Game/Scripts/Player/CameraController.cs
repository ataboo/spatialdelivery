﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Camera mainCamera;
    private float zoom = 40;
    public float zoomSpeed = 1f;
    public Vector2 zoomRange = new Vector2(10, 140);
    private bool bodyUp = false;
    public float maxCamRotation = 1f;
    public GameObject backgroundImage;
    public float mapZoom = 200;
    private SpriteRenderer backgroundImgSprite;

    public AnimationCurve backgroundZoomRate;
    private MapController mapController;
    private PlayerController player;
    public AudioSource toastAudioSource;

    void Start() {
        mapController = GetComponent<MapController>();
        backgroundImgSprite = backgroundImage.GetComponent<SpriteRenderer>();

        player = SceneContext.instance.Player();

        mainCamera = GetComponent<Camera>();
    }

    public void PlayToastAudio(AudioClip clip) {
        toastAudioSource.clip = clip;
        toastAudioSource.Play();
    }

    void LateUpdate() {
        if (target == null) {
            return;   
        }

        if (Input.GetButtonDown("Map")) {
            if (zoom > mapZoom) {
                zoom = 30;
            } else {
                zoom = zoomRange.y / 2;
            }
        } else {
            zoom = Mathf.Clamp(zoom - Input.GetAxis("ZoomCam") * zoomSpeed * zoom, zoomRange.x, zoomRange.y);
        }

        mainCamera.orthographicSize = zoom;

        bool showMap = zoom > mapZoom;
        if (mapController.MapOn != showMap) {
            mapController.SetMapOn(showMap);
        }

        if (bodyUp) {
            //
        } else {
            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * maxCamRotation);
        }

        transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

        float t = (zoom - zoomRange.x) / (zoomRange.y - zoomRange.x);
        float diagonal = backgroundZoomRate.Evaluate(t) * (zoomRange.y - zoomRange.x) + zoomRange.x;

        diagonal *= Mathf.Max(Screen.width/Screen.height, 1) * 4;
        backgroundImage.transform.position = new Vector3(transform.position.x, transform.position.y, backgroundImage.transform.position.z);
        // ~ root(2) * 2 for diagonal.
        backgroundImage.transform.localScale = new Vector3(diagonal, diagonal, 1); 
    }

    float EaseOut(float min, float max, float t) {
        float sinT = Mathf.Sin(t * Mathf.PI / 2f);

        return (max - min) * sinT + min;
    }

    float EaseOutExp(float min, float max, float t) {
        float expT = t * t;

        return (max - min) * expT + min;
    }

    float EaseIn(float min, float max, float t) {
        float cosT = 1f - Mathf.Cos(t * Mathf.PI / 2f);

        return (max - min) * cosT + min;
    }
}
