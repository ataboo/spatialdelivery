﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GunnerSkill {
    [System.Serializable]
    public enum Level {
        Rookie,
        Intermediate,
        Advanced,
        Expert
    }

    private Level level;
    private const float deviationDegPerSecond = 10f;
    private int direction = 0;
    private SeedRandom rand;

    public GunnerSkill(Level level) {
        this.level = level;
        this.rand = new SeedRandom();

        direction = rand.Bool() ? 1 : -1;
    }

    public float MaxDeviation() {
        switch(level) {
            case Level.Rookie:
                return 10f;
            case Level.Intermediate:
                return 5f;
            case Level.Advanced:
                return 2f;
            case Level.Expert:
                return 0.2f;
        }

        throw new System.NotImplementedException("invalid skill level");
    }
    public float DeviateAngle(float targetAngle, float lastAngle, float delta) {
        float nextAngle = lastAngle + direction * delta * deviationDegPerSecond;
        float deltaAngle = AtaMath.DeltaAngleSigned(targetAngle, nextAngle);
        
        if (Mathf.Sign(deltaAngle) == Mathf.Sign(direction)) {
            if (rand.NextDouble() < Mathf.Abs(deltaAngle) / MaxDeviation() * delta * 10f) {
                direction *= -1;
            }
        }

        if (deltaAngle < -MaxDeviation()) {
            direction = 1;
            nextAngle = AtaMath.NormalizeAngle(targetAngle - MaxDeviation());
        } else if (deltaAngle > MaxDeviation()) {
            direction = -1;
            nextAngle = AtaMath.NormalizeAngle(targetAngle + MaxDeviation());
        }

        return nextAngle;
    }
}
