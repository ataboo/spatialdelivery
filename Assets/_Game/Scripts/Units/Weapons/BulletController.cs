using UnityEngine;

// Fired from hardpoints that use ballistics
public class BulletController: MonoBehaviour {
    public float lifeSpan = 5;
    public GameObject explosionPrefab;
    private PhysicsBody firingBody;
    private float damage;
    
    void Start() {
        GameObject.Destroy(gameObject, lifeSpan);
    }

    private void SetVelocity(Vector2 velocity) {
        GetComponent<Rigidbody2D>().velocity = velocity;
    }

    public void ShootFromBody(PhysicsBody body, float damage, float muzzleVelocity) {
        SetVelocity(body.Velocity() + (Vector2)(transform.up * muzzleVelocity));
        firingBody = body;
        this.damage = damage;
    }

    void OnTriggerEnter2D(Collider2D col) {
        if ((firingBody != null && col.transform.root.gameObject == firingBody.gameObject) || col.isTrigger) {
            return;
        }

        bool hit = false;
        UnitController unit = col.GetComponentInParent<UnitController>();
        if (unit != null) {
            hit = true;
            unit.TakeDamage(damage);
        } else {
            Debug.LogWarningFormat("{0} has no DamageReceiver", col.gameObject.name);
        }

        Vector3 hitPoint = col.bounds.ClosestPoint(transform.position) + transform.up * 0.3f;
        var explosion = GameObject.Instantiate(explosionPrefab, hitPoint + 1*transform.forward, transform.rotation);
        
        var expController = explosion.GetComponent<BulletExplosionController>();
        if (hit) {
            expController.PlayHit();
        } else {
            expController.PlayMiss();
        }
        GameObject.Destroy(explosion, 3f);
        GameObject.Destroy(gameObject);
    }
}