using UnityEngine;

public abstract class ProjectileHardpointController: HardpointController {
    public float muzzleVelocity = 30f;

    public override AtaMath.InterceptSolution GetInterceptSolution(PhysicsBody targetBody) {
        // var solution = AtaMath.CalculateIntercept(physicsBody.transform.position, muzzleVelocity, targetBody.transform.position, targetBody.Rigidbody().velocity);
        var solution = AtaMath.CalculateIntercept(physicsBody, targetBody, muzzleVelocity);

        if (solution != null) {
            Debug.DrawLine(physicsBody.transform.position, solution.hitPos, Color.magenta);
        }

        return solution;
    }
}