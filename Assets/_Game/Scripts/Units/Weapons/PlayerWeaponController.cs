using UnityEngine;

public class PlayerWeaponController: WeaponController {
    public override void SetActive(bool active) {
        ForAllTurrets((turret) => {
            if (active) {
                turret.TrackCursorMode();
            } else {
                turret.StopTracking();
            }
            turret.SetActive(active);
        });
    }

    public override void Initialize() {
        SetActive(false);
        TrackCursorMode();
        StopFiring();
    }

    public void TrackCursorMode() {
        target = null;

        ForAllTurrets((turret)=>{
            turret.TrackCursorMode();
        });
    }

    override public void StopTracking() {
        TrackCursorMode();
    }

    public bool TargetClosestVehicle() {
        var colliders = Physics2D.OverlapCircleAll(radar.transform.position, radar.radius, LayerMask.GetMask("Vehicle"));

        float closestRangeSqr = float.MaxValue;
        PhysicsBody body = null;

        Debug.LogFormat("Found colliders: {0}", colliders.Length);

        foreach(Collider2D col in colliders) {
            float range = (col.transform.position - radar.transform.position).sqrMagnitude;
            PhysicsBody colBody = col.GetComponent<PhysicsBody>();

            if (colBody == null || colBody == physics) {
                continue;
            }

            if (range < closestRangeSqr || body == null) {
                body = colBody;
                closestRangeSqr = range;
            }
        }

        if (body == null || body == target) {
            StopTracking();
            return false;
        }

        TrackTarget(body);
        return true;
    }
}