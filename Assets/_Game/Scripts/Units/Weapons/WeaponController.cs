using UnityEngine;
using System;

// Unit will use this to control the turret controllers
abstract public class WeaponController: MonoBehaviour {
    public CircleCollider2D radar;
    public GunnerSkill.Level gunnerSkillLevel = GunnerSkill.Level.Intermediate;
    
    protected TurretController[] turrets;
    protected UnitController unit;
    protected PhysicsBody physics;
    protected PhysicsBody target;

    public enum State {
        Unlocked,
        Locked,
        Off,
        Disabled
    }

    public PhysicsBody Target {
        get {
            return target;
        }
    }

    public State WeaponState {
        get {
            if (turrets.Length == 0) {
                return State.Disabled;
            }

            if (Active) {
                if (target != null) {
                    return State.Locked;
                } else {
                    return State.Unlocked;
                }
            }

            return State.Off;
        }
    }

    public bool Active {
        get {
            if (turrets.Length == 0) {
                return false;
            }

            return turrets[0].Active;
        }
    }

    void FixedUpdate() {
        if (turrets.Length == 0) {
            return;
        }

        if (target == null) {
            if (turrets[0].TrackMode == TurretController.Tracking.Target) {
                StopTracking();
            }
        } else if (!TargetInRange()) {
            StopTracking();
        }

        ForAllTurrets((turret) => {
            turret.UpdateTracking();
            turret.UpdateFiring();
        });
    }

    bool TargetInRange() {
        float rangeSqr = (target.transform.position - radar.transform.position).sqrMagnitude;
        return rangeSqr < radar.radius * radar.radius;
    }

    public abstract void SetActive(bool active);

    void Start() {
        this.unit = GetComponent<UnitController>();
        turrets = GetComponentsInChildren<TurretController>();
        physics = GetComponent<PhysicsBody>();

        ForAllTurrets((turret) => {
            turret.SetSkillLevel(gunnerSkillLevel);
        });
    }

    public abstract void Initialize();

    public void ToggleActive() {
        SetActive(!Active);
    }

    public void FireWithLOS() {
        ForAllTurrets((turret) => {
            turret.SetFireMode(TurretController.FireMode.RequireLOS);
        });
    }

    public void Fire() {
        if(!Active) {
            return;
        }

        ForAllTurrets((turret) => {
            turret.SetFireMode(TurretController.FireMode.Fire);
        });
    }

    public virtual void StopFiring() {
        ForAllTurrets((turret)=>{
            turret.SetFireMode(TurretController.FireMode.Hold);
        });
    }

    public virtual void StopTracking() {
        target = null;
        
        ForAllTurrets((turret) => {
            turret.StopTracking();
        });
    }

    public void TrackTarget(PhysicsBody body) {
        if (body == null) {
            StopTracking();
            return;
        }

        target = body;

        ForAllTurrets((turret)=>{
            turret.TrackTargetMode(body);
        });
    }

    protected void ForAllTurrets(TurretDelegate turretDelegate) {
        foreach(TurretController turret in turrets) {
            turretDelegate(turret);
        };
    }

    protected delegate void TurretDelegate(TurretController turret);
}