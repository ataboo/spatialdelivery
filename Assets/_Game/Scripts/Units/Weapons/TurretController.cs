﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Turret holding a weapon on a hardpoint.  Controlled by a Unit's WeaponController.
public class TurretController : MonoBehaviour
{
    public enum Tracking {
        Cursor,
        Target,
        None
    }

    public enum FireMode {
        Hold,
        Fire,
        RequireLOS
    }

    private HardpointController hardpoint;

    private GunnerSkill gunnerSkill;

    [Range(-180f, 180f)]
    public float lowerLimit;
    [Range(-180f, 180f)]
    public float upperLimit;
    public bool showGizmos;

    float lowerNormalized;
    float upperNormalized;
    Vector2 rangeRads;
    PhysicsBody target;
    PhysicsBody vehicleBody;
    bool active = false;

    public bool Armed {
        get {
            return active && trackMode != Tracking.None;
        }
    }

    public bool Active {
        get {
            return active;
        }
    }

    public Tracking TrackMode {
        get {
            return trackMode;
        }
    }

    private Tracking trackMode = Tracking.None;
    FireMode fireMode = FireMode.Hold;

    void Awake() {
        hardpoint = transform.GetComponentInChildren<HardpointController>();
        vehicleBody = GetComponentInParent<PhysicsBody>();

        lowerNormalized = AtaMath.NormalizeAngle(lowerLimit);
        upperNormalized = AtaMath.NormalizeAngle(upperLimit);
    }

    public void UpdateTracking() {
        if (!active) {
            return;
        }
        
        switch(trackMode) {
            case Tracking.Cursor:
                TrackCursorUpdate();
                break;
            case Tracking.Target:
                TrackTargetUpdate();
                break;
            default:
                break;
        }
    }

    public bool UpdateFiring() {
        return hardpoint.UpdateFiring(fireMode);
    }

    public void SetSkillLevel(GunnerSkill.Level skillLevel) {
        this.gunnerSkill = new GunnerSkill(skillLevel);
    }

    public void TrackTargetMode(PhysicsBody target) {
        this.target = target;
        this.trackMode = Tracking.Target;
        UpdateTracking();
    }

    public void TrackCursorMode() {
        this.trackMode = Tracking.Cursor;
    }

    public void StopTracking() {
        this.trackMode = Tracking.None;
        hardpoint.SetSolution(false, false);
        AimAtAngle(0);
    }

    public void SetFireMode(FireMode fireMode) {
        this.fireMode = fireMode;
    }

    void TrackCursorUpdate() {
        var cursorPos = SceneContext.instance.MainCamera().ScreenToWorldPoint(Input.mousePosition);
        bool inArc = AimAtPoint(cursorPos);
        if (!inArc) {
            AimAtAngle(0);
        }

        hardpoint.SetSolution(inArc, true);
    }

    void TrackTargetUpdate() {
        bool inArc = TryAimAtTarget();
        if (!inArc) {
            AimAtAngle(0);
        }

        bool losToTarget = false;
        if (fireMode == FireMode.RequireLOS) {
            losToTarget = HasLineOfSiteToTarget();
        }

        hardpoint.SetSolution(inArc, losToTarget);
    }

    bool TryAimAtTarget() {
        if (target == null) {
            return false;
        }

        AtaMath.InterceptSolution solution = hardpoint.GetInterceptSolution(target);
        if (solution == null) {
            return false;
        }

        return AimAtPoint(solution.hitPos);
    }

    public void SetActive(bool active) {
        this.active = active;
    }

    bool HasLineOfSiteToTarget() {
        var hit = hardpoint.CastFromCenterToTarget(target);

        if (hit.collider == null) {
            return false;
        }

        if (hit.collider.isTrigger) {
            Debug.LogWarningFormat(
                "Line of site check shouldn't be hitting triggers... fix the layers! gameObject: {0} with root: {1}", 
                hit.collider.name, 
                hit.collider.transform.root.name
            );
        }

        PhysicsBody physics = hit.collider.GetComponent<PhysicsBody>();
        if (physics == null) {
            return false;
        }

        return physics.gameObject == target.gameObject;
    }

    bool AimAtAngle(float angle, bool checkArcLimits = true) {
        if (gunnerSkill == null) {
            Debug.LogErrorFormat("Turret {0}'s gunner skill should have been set by its weapon controller", name);
            return false;
        }

        angle = gunnerSkill.DeviateAngle(angle, -hardpoint.transform.localRotation.eulerAngles.z, Time.fixedDeltaTime);

        if (checkArcLimits) {
            var withinArcLimits = AtaMath.AngleInRange(angle, lowerNormalized, upperNormalized, false);
            if (!withinArcLimits) {
                return false;
            }
        }
        
        hardpoint.transform.localRotation = Quaternion.Euler(0, 0, -angle);
        
        return true;
    }

    bool AimAtPoint(Vector2 point) {
        var bearingVect = point - (Vector2)transform.position;
        float bearing = Vector2.SignedAngle(new Vector2(bearingVect.x, bearingVect.y), transform.up);

        bearing = AtaMath.NormalizeAngle(bearing);

        return AimAtAngle(bearing);
    }

    void OnDrawGizmosSelected() {
        if (!showGizmos) {
            return;
        }

        Gizmos.color = Color.red;

        Vector3 nextPos;
        Vector3 lastPos = GizmoAnglePoint(lowerLimit);
        float span = (upperLimit - lowerLimit + 360f) % 360f;
        Gizmos.DrawLine(transform.position, lastPos);
        for(int i=0; i<span; i++) {
            nextPos = GizmoAnglePoint((float)i + lowerLimit);
            Gizmos.DrawLine(lastPos, nextPos);
            lastPos = nextPos;
        }

        nextPos = GizmoAnglePoint(upperLimit);
        Gizmos.DrawLine(lastPos, nextPos);
        Gizmos.DrawLine(nextPos, transform.position);
    }

    Vector3 GizmoAnglePoint(float angle) {
        float gizmoRadius = 10f;
        
        return transform.position + Quaternion.Euler(0, 0, -angle) * transform.up * gizmoRadius;
    }
}
