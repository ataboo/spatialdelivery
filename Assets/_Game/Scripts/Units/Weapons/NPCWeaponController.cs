using UnityEngine;

public class NPCWeaponController: WeaponController {
    public override void Initialize() {
        SetActive(true);
        StopFiring();
    }

    public override void SetActive(bool active) {
        ForAllTurrets((turret) => {
            turret.SetActive(active);
        });
    }

    public PhysicsBody FindClosestTarget<T>(bool ignoreAllied = true, bool castLineOfSite = true) where T: UnitController {
        Collider2D[] results = new Collider2D[32];
        Physics2D.OverlapCollider(radar, new ContactFilter2D(), results);

        PhysicsBody closestBody = null;
        float closestMagSqr = float.MaxValue;

        foreach(Collider2D col in results) {
            if (col == null || col.isTrigger || col.transform.root.gameObject == unit.gameObject) {
                continue;
            }

            T component = col.GetComponent<T>();
            if (component == null) {
                continue;
            }

            if(ignoreAllied && unit.Faction.AlliedTo(component.Faction)) {
                continue;
            }

            PhysicsBody body = col.GetComponent<PhysicsBody>();
            if (body == null) {
                continue;
            }

            if (castLineOfSite && !HasLineOfSite(body)) {
                continue;
            }

            Vector2 deltaPos = body.transform.position - unit.transform.position;
            if (deltaPos.sqrMagnitude < closestMagSqr) {
                closestBody = body;
            }
        }

        return closestBody;
    }

    bool TargetValid() {
        if (target == null) {
            return false;
        }

        Vector2 deltaPos = target.transform.position - unit.transform.position;

        if (deltaPos.sqrMagnitude > radar.radius * radar.radius) {
            return false;
        }

        return HasLineOfSite(target);
    }

    public bool TrackClosestTarget<T>() where T: UnitController {
        if (!TargetValid()) {
            target = FindClosestTarget<T>();
        }
        TrackTarget(target);
        
        return target != null;
    }

    public bool HasLineOfSite(PhysicsBody body) {
        var hits = Physics2D.RaycastAll(physics.transform.position, (body.transform.position - physics.transform.position).normalized, radar.radius);
        float targetRange = -1;
        float closestNonTargetRange = float.MaxValue;

        foreach(RaycastHit2D hit in hits) {
            if (hit.collider.isTrigger) {
                continue;
            }

            var rootObject = hit.collider.transform.root.gameObject;
            if (rootObject == physics.gameObject) {
                continue;
            }

            var hitBody = hit.collider.GetComponentInParent<PhysicsBody>();

            if (hitBody == body) {
                targetRange = hit.distance;
                continue;
            }

            closestNonTargetRange = Mathf.Min(closestNonTargetRange, hit.distance);
        }

        if (targetRange < 0) {
            Debug.LogWarning("Line of site can't find target");
            return false;
        }

        return closestNonTargetRange > targetRange;
    }
}