﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplosionController : MonoBehaviour
{
    public AudioSource missAudio;
    public AudioSource hitAudio;

    public void PlayMiss() {
        missAudio.pitch = Random.Range(0.95f, 1.05f);
        missAudio.Play();
    }

    public void PlayHit() {
        missAudio.pitch = Random.Range(0.95f, 1.05f);
        hitAudio.Play();
    }
}
