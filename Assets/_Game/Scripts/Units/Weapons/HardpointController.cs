using UnityEngine;

// Parent class to weapons
public abstract class HardpointController : MonoBehaviour {
    public Transform muzzle;

    protected PhysicsBody physicsBody;
    protected LayerMask castHitLayers;
    public float maxRange = 200f;
    protected bool losToTarget = false;
    protected bool targetInArc = false;

    protected virtual void Awake() {
        physicsBody = GetComponentInParent<PhysicsBody>();
        castHitLayers = LayerMask.GetMask("Vehicle", "OrbitalBody", "Default");
    }

    abstract protected bool TryFire(TurretController.FireMode fireMode);

    public virtual bool UpdateFiring(TurretController.FireMode fireMode) {
        return TryFire(fireMode);
    }

    public abstract AtaMath.InterceptSolution GetInterceptSolution(PhysicsBody targetBody);

    public RaycastHit2D CastFromMuzzleForUnit() {
        return Physics2D.Raycast(muzzle.position, muzzle.up, maxRange, castHitLayers);
    }

    public RaycastHit2D CastFromCenterToTarget(PhysicsBody targetBody, float selfMargin = 1f) {
        Vector2 targetBearing = (targetBody.transform.position - transform.position).normalized;
        Vector2 castStart = (Vector2)transform.position + targetBearing * selfMargin;
        RaycastHit2D hit = Physics2D.Raycast(castStart, targetBearing, maxRange, castHitLayers);

        return hit;
    }

    public void SetSolution(bool targetInArc, bool losToTarget) {
        this.targetInArc = targetInArc;
        this.losToTarget = losToTarget;
    }
}