using UnityEngine;
using System;
using System.Collections;


public class CannonController: ProjectileHardpointController {
    public GameObject bulletPrefab;
    public float fireDelay = 0.2f;
    public float damage = 10f;
    bool onCooldown = false;
    AudioSource fireAudio;

    override protected void Awake() {
        base.Awake();

        fireAudio = GetComponent<AudioSource>();
        fireAudio.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
    }

    override protected bool TryFire(TurretController.FireMode fireMode) {
        if (fireMode == TurretController.FireMode.Hold || !targetInArc || onCooldown) {
            return false;
        }

        if (fireMode == TurretController.FireMode.RequireLOS && !losToTarget) {
            return false;
        }
        
        SpawnRound();
        StartCoroutine(GunTimeout());
        
        return true;
    }

    private IEnumerator GunTimeout() {
        onCooldown = true;
        yield return new WaitForSeconds(fireDelay);
        onCooldown = false;
        yield return null;
    }

    private void SpawnRound() {
        fireAudio.Play();
        var bulletObj = GameObject.Instantiate(bulletPrefab, muzzle.position, transform.rotation);
        var bulletControl = bulletObj.GetComponent<BulletController>();
        bulletControl.ShootFromBody(physicsBody, damage, muzzleVelocity);
    }
}