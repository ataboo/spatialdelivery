using UnityEngine;
using Vectrosity;
using System.Collections.Generic;

public class LaserController: HardpointController {
    public float dps = 10f;
    Color laserColor = Color.red;
    public ParticleSystem hitParticles;
    public AudioSource hitSound;
    public AudioSource beamSound;
    public MeshRenderer beamRenderer;
    public MeshFilter beamFilter;
    BeamRender beamRender;
    public float beamWidth = 0.25f;

    protected void Start() {
        beamRender = new BeamRender(beamFilter, beamRenderer, beamWidth);
        beamRender.SetEnabled(false);

        beamSound.pitch = Random.Range(0.9f, 1.1f);
        hitSound.pitch = Random.Range(0.9f, 1.1f);

        ToggleBeamEffects(false);
        ToggleDamageEffects(false);
    }

    protected override bool TryFire(TurretController.FireMode fireMode)
    {
        if (TryLaserFire(fireMode)) {
            ToggleBeamEffects(true);
            return true;
        }

        ToggleBeamEffects(false);
        ToggleDamageEffects(false);

        return false;
    }

    bool TryLaserFire(TurretController.FireMode fireMode) {
        if (fireMode == TurretController.FireMode.Hold || !targetInArc) {
            return false;
        }

        if (fireMode == TurretController.FireMode.RequireLOS && !losToTarget) {
            return false;
        }

        bool hitTarget = false;

        // We're casting again because the original on target check doesn't take aim variance into account.
        var hit = CastFromMuzzleForUnit();
        if (hit.collider == null) {
            beamRender.UpdateMeshPoints(maxRange);
        } else {
            beamRender.UpdateMeshPoints((hit.point - (Vector2)transform.position).magnitude);
            hitTarget= true;
            hitParticles.transform.position = hit.point;
            var hitUnit = hit.collider.GetComponent<UnitController>();
            if (hitUnit != null) {
                hitUnit.TakeDamage(dps * Time.deltaTime);
            }
        }

        ToggleDamageEffects(hitTarget);

        return true;
    }

    void ToggleDamageEffects(bool on) {
        var emission = hitParticles.emission;
        emission.enabled = on;

        hitSound.enabled = on;
    }

    void ToggleBeamEffects(bool on) {
        beamRender.SetEnabled(on);
        beamSound.enabled = on;
    }

    public override AtaMath.InterceptSolution GetInterceptSolution(PhysicsBody targetBody) {
        return new AtaMath.InterceptSolution(0, targetBody.transform.position);
    }

    public class BeamRender {
        float beamWidth = 0.25f;
        Mesh mesh;
        MeshFilter meshFilter;
        MeshRenderer meshRenderer;
        Vector3 bl;
        Vector3 br;
        Vector3 tl;
        Vector3 tr;

        public BeamRender(MeshFilter meshFilter, MeshRenderer meshRenderer, float beamWidth) {
            this.meshFilter = meshFilter;
            this.meshRenderer = meshRenderer;
            this.beamWidth = beamWidth;
            mesh = new Mesh();
            mesh.name = "Laser Beam";
            meshFilter.mesh = mesh;
        }

        public void UpdateMeshPoints(float length) {
            bl = -Vector2.right * (beamWidth/2.5f);
            br = Vector2.right * (beamWidth/2.5f);
            tl = Vector2.up * length - Vector2.right * (beamWidth/1.5f);
            tr = Vector2.up * length + Vector2.right * (beamWidth/1.5f);

            meshFilter.mesh.vertices = new Vector3[]{bl, br, tl, tr};
            meshFilter.mesh.triangles = new int[]{0, 2, 1, 1, 2, 3};
            // meshFilter.mesh.uv = new Vector2[]{Vector2.zero, new Vector2(0.2f, 0), new Vector2(0, 500), new Vector2(0.2f, 50)};

            // Scale the uvs?  
        }

        public void SetEnabled(bool on) {
            meshRenderer.enabled = on;
        }
    }
}