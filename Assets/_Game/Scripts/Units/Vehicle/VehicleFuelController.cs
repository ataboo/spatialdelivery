using UnityEngine;


public class VehicleFuelController {
    RefuelController currentRefueler;
    float fuelRemaining;
    Config config;
    VehicleController vehicle;

    public float FuelRemaining {
        get {
            return fuelRemaining;
        }
    }

    public float FuelFraction {
        get {
            return fuelRemaining/config.capacity;
        }
    }

    public SpaceportController CurrentSpaceport {
        get {
            if (currentRefueler != null) {
                return currentRefueler.SpacePort;
            }

            return null;
        }
    }

    public VehicleFuelController(Config config, VehicleController vehicle) {
        this.config = config;
        this.vehicle = vehicle;

        fuelRemaining = config.capacity;
    }

    public void SetRefueler(RefuelController refueler) {
        if (refueler == null) {
            if (currentRefueler != null) {
                currentRefueler.CheckOut(vehicle);
            }
        } else {
            refueler.CheckIn(vehicle);
        }
        
        currentRefueler = refueler;
    }

    public void RefuelTick(float delta) {
        if (currentRefueler != null) {
            fuelRemaining = Mathf.Min(config.capacity, fuelRemaining + currentRefueler.refuelRate * delta);
        }
    }

    public void BurnFuel(Vector3 input, float delta) {
        var rate = config.consumption;

        float consumptionRate = Mathf.Abs(input.x) * rate.x + Mathf.Abs(input.y) * rate.y + Mathf.Abs(input.z) * rate.z;
        fuelRemaining = Mathf.Max(0f, fuelRemaining - consumptionRate * delta);
    }


    [System.Serializable]
    public class Config {
        public float capacity = 100f;
        public Vector3 consumption = new Vector3(1f, 1f, 0.5f);
    }
}