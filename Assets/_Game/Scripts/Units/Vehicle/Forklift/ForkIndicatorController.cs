using UnityEngine;

public class ForkIndicatorController : MonoBehaviour {
    public Sprite redLight;
    public Sprite greenLight;
    public GameObject rightClamp;
    public GameObject leftClamp;
    private SpriteRenderer rightRenderer;
    private SpriteRenderer leftRenderer;

    public enum State {
        OFF,
        READY,
        LOCKED
    }

    void Awake() {
        rightRenderer = rightClamp.GetComponent<SpriteRenderer>();
        leftRenderer = leftClamp.GetComponent<SpriteRenderer>();
    }

    public void SetLeft(State state) {
        SetIndicatorState(leftRenderer, state);
    }

    public void SetRight(State state) {
        SetIndicatorState(rightRenderer, state);
    }

    private void SetIndicatorState(SpriteRenderer renderer, State state) {
        switch(state) {
            case State.READY:
                renderer.sprite = redLight;
                break;
            case State.LOCKED:
                renderer.sprite = greenLight;
                break;
            case State.OFF: 
            default:
                renderer.sprite = null;
                break;
        }
    }
}