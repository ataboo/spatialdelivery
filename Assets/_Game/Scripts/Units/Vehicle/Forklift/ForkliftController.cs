﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkliftController : VehicleController
{
    private ForkClampController forkClamps;
    public FixedJoint2D loadJoint;
    private Animator animator;

    protected override void Awake() {
        base.Awake();

        forkClamps = GetComponentInChildren<ForkClampController>();
        animator = GetComponentInChildren<Animator>();
    }

    public override void ToggleLandingGear() {
        if (CurrentLoad() != null || landingControl.CurrentLockedbody != null) {
            return;
        }

        var gearDown = !GearDown();
        animator.SetBool("gearDown", gearDown);

        if (gearDown) {
            IndicateLandingGear();
        } else {
            IndicateLoadClamps();
        }
    }

    private void IndicateLoadClamps() {
        landingControl.SetLandingLegIndicationEnabled(false);
        
        forkClamps.enabled = true;
        forkClamps.UpdateIndication();
    }

    private void IndicateLandingGear() {
        forkClamps.enabled = false;
        
        landingControl.SetLandingLegIndicationEnabled(true);
    }

    public override void TogglePayload() {
        if (GearDown()) {
            return;
        }

        if (CurrentLoad() == null) {
            TryGrabPayload();
        } else {
            DropPayload();
        }
    }

    public override bool GearDown() {
        return animator.GetBool("gearDown");
    }

    private void TryGrabPayload() {
        var payload = forkClamps.TryGrabPayload();
        if (payload == null) {
            return;
        }

        var payloadController = payload.GetComponent<PayloadController>();
        payloadController.DetachFromBulkCarrier();
        payloadController.AttachToForklift(satelliteBody);
        
        payload.velocity = satelliteBody.Velocity();
        loadJoint.connectedBody = payload;
        loadJoint.enabled = true;
        forkClamps.UpdateIndication();
    }

    private void DropPayload() {
        if (CurrentLoad() == null) {
            return;
        }

        var payload = loadJoint.connectedBody.GetComponent<PayloadController>();
        payload.DetachFromForklift(satelliteBody);
        loadJoint.connectedBody = null;
        loadJoint.enabled = false;
        forkClamps.DropPayload();
    }

    public override Rigidbody2D CurrentLoad() {
        return loadJoint.connectedBody;
    }
}
