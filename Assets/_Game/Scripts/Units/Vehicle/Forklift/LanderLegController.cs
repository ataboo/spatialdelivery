﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ForkState = ForkIndicatorController.State;

public class LanderLegController : MonoBehaviour, Trigger2DDelegate
{
    public DelegatingTrigger leftLeg;
    public DelegatingTrigger rightLeg;
    private PhysicsBody leftLegOn = null;
    private PhysicsBody rightLegOn = null;
    private PhysicsBody bothLegsOn = null;
    public ForkIndicatorController forkIndicator;
    private bool lockedLanding;

    void Start() {
        leftLeg.triggerDelegate = this;
        rightLeg.triggerDelegate = this;
    }

    void Trigger2DDelegate.DelTriggerEnter2D(Collider2D col, DelegatingTrigger sender) {
        if (col.gameObject == gameObject) {
            return;
        }

        if (lockedLanding || !this.enabled) {
            return;
        }

        var body = col.GetComponent<PhysicsBody>();

        if (sender == leftLeg) {
            leftLegOn = body;
        } else if (sender == rightLeg) {
            rightLegOn = body;
        }

        UpdateBothLegsOn();
        UpdateTouchingIndication();
    }

    void Trigger2DDelegate.DelTriggerExit2D(Collider2D col, DelegatingTrigger sender) {
        if (col.gameObject == gameObject) {
            return;
        }

        if (lockedLanding || !this.enabled) {
            return;
        }

        if (sender == leftLeg) {
            leftLegOn = null;
        } else if (sender == rightLeg) {
            rightLegOn = null;
        }

        bothLegsOn = null;
        UpdateTouchingIndication();
    }

    public PhysicsBody SittingOnObject() {
        return bothLegsOn;
    }

    public void IndicateLanded(bool landed) {
        lockedLanding = landed;

        if (landed) {
            forkIndicator.SetLeft(ForkState.LOCKED);
            forkIndicator.SetRight(ForkState.LOCKED);
        } else {
            UpdateTouchingIndication();
        }
    }

    public void RefreshColliders() {
        if (leftLegOn != null) {
            if (!leftLeg.ContainsBody(leftLegOn)) {
                leftLegOn = null;
            }
        }

        if (rightLegOn != null) {
            if (!rightLeg.ContainsBody(rightLegOn)) {
                rightLegOn = null;
            }
        }

        UpdateBothLegsOn();
        UpdateTouchingIndication();
    }

    void UpdateBothLegsOn() {
        if (leftLegOn != null && rightLegOn == leftLegOn) {
            bothLegsOn = leftLegOn;
        } else {
            bothLegsOn = null;
        }
    }

    public void UpdateTouchingIndication() {
        forkIndicator.SetLeft(leftLegOn == null ? ForkState.OFF : ForkState.READY);
        forkIndicator.SetRight(rightLegOn == null ? ForkState.OFF : ForkState.READY);
    }
}
