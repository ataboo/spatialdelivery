﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ForkState = ForkIndicatorController.State;

public class ForkClampController : MonoBehaviour, Trigger2DDelegate {
    private Rigidbody2D leftInRange = null;
    private Rigidbody2D rightInRange = null;
    private int lugLayer;
    private VehicleController vehicle;
    private string lugLayerName = "PickupLug";
    public GameObject rightClamp;
    public GameObject leftClamp;
    public ForkIndicatorController forkIndicator;

    void Start() {
        lugLayer = LayerMask.NameToLayer(lugLayerName);
        vehicle = GetComponentInParent<VehicleController>();
        rightClamp.GetComponent<DelegatingTrigger>().triggerDelegate = this;
        leftClamp.GetComponent<DelegatingTrigger>().triggerDelegate = this;
    }

    public void DelTriggerEnter2D(Collider2D col, DelegatingTrigger sender)
    {
        if (col.gameObject.layer != lugLayer || !this.enabled) {
            return;
        }

        if (sender.gameObject == rightClamp) {
            rightInRange = col.attachedRigidbody;
        } else {
            leftInRange = col.attachedRigidbody;
        }

        UpdateIndication();
    }

    public void DelTriggerExit2D(Collider2D col, DelegatingTrigger sender)
    {
        if (col.gameObject.layer != lugLayer || !this.enabled) {
            return;
        }

        if (sender.gameObject == rightClamp) {
            rightInRange = null;
        } else {
            leftInRange = null;
        }

        UpdateIndication();
    }

    public void UpdateIndication() {
        if (vehicle.CurrentLoad() != null) {
            forkIndicator.SetLeft(ForkState.LOCKED);
            forkIndicator.SetRight(ForkState.LOCKED);
        } else {
            forkIndicator.SetLeft(leftInRange ? ForkState.READY : ForkState.OFF);
            forkIndicator.SetRight(rightInRange ? ForkState.READY : ForkState.OFF);
        }

    }

    public Rigidbody2D TryGrabPayload() {
        if (leftInRange == null || leftInRange != rightInRange) {
            return null;
        }
        
        return leftInRange;
    }

    public void DropPayload() {
        UpdateIndication();
    }
}
