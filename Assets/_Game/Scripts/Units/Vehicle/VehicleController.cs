﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AtaBehaviour;

// SatelliteBody Unit
public abstract class VehicleController : UnitController
{
    public FixedJoint2D landingJoint;
    public float turnSpeed = 10f;
    public float thrustForce = 10f;
    public PhysicsBody startSittingOn;

    public VehicleFuelController.Config fuelConfig;
    public QuestEventDelegate landingEventDelegate;

    protected ThrusterController thrusterController;
    protected SatelliteBody satelliteBody;
    protected VehicleLandingControl landingControl;
    protected VehicleFuelController fuelControl;
    protected Autopilot pilot;
    private CollisionDetector colliisionDetector;

    public SpaceportController CurrentSpaceport {
        get {
            return fuelControl.CurrentSpaceport;
        }
    }

    public Autopilot Pilot {
        get {
            return pilot;
        }
    }

    public CollisionDetector Collision {
        get {
            return colliisionDetector;
        }
    }

    public SatelliteBody SatBody {
        get {
            return satelliteBody;
        }
    }

    public bool Landed {
        get {
            return landingJoint.enabled;
        }
    }

    override protected void Awake() {
        base.Awake();
        
        thrusterController = GetComponentInChildren<ThrusterController>();
        
        satelliteBody = GetComponent<SatelliteBody>();
        fuelControl = new VehicleFuelController(fuelConfig, this);

        var landConfig = new VehicleLandingControl.Config();
        landConfig.landingJoint = landingJoint;
        landConfig.legController = GetComponentInChildren<LanderLegController>();
        landConfig.startSittingOn = startSittingOn;
        landConfig.fuelControl = fuelControl;
        landConfig.overlay = GetComponent<OrbitalOverlay>();
        landingControl = new VehicleLandingControl(landConfig);

        pilot = GetComponent<Autopilot>();
        colliisionDetector = GetComponent<CollisionDetector>();
    }

    void FixedUpdate() {
        if (landingControl.CurrentLockedbody != null) {
            fuelControl.RefuelTick(Time.fixedDeltaTime);
        }
    }

    public void InputUpdate(Vector3 input) {
        input = input.normalized;

        if (fuelControl.FuelRemaining == 0f || landingControl.CurrentLockedbody != null) {
            input = Vector3.zero;
        } else {
            Physics.Rigidbody().AddTorque(input.z * -turnSpeed);

            Physics.Rigidbody().AddForce(transform.right * input.x * thrustForce);
            Physics.Rigidbody().AddForce(transform.up * input.y * thrustForce);

            fuelControl.BurnFuel(input, Time.fixedDeltaTime);
        }

        thrusterController.SetThrusters(input);
        audioController.SetThrusterLevel(input.magnitude);
    }

    public virtual void ToggleLandingGear() {
        //
    }

    public virtual void TogglePayload() {
        //
    }

    public virtual void TrackWeaponTarget() {

    }

    public void MoveToLandingPoint(Transform landingPoint) {
        transform.rotation = landingPoint.rotation;
        transform.position = (Vector2)landingPoint.position - (Vector2)(landingPoint.rotation * landingJoint.anchor);
    }

    public virtual VehicleController NextShipAtSpaceport() {
        if (CurrentSpaceport != null) {
            var nextVehicle = CurrentSpaceport.NextVehicle(this);

            Debug.LogFormat("Next vehicle: {0}", nextVehicle);

            return nextVehicle;
        }

        return null;
    }

    public virtual bool GearDown() {
        return true;
    }

    public virtual Rigidbody2D CurrentLoad() {
        return null;
    }

    public float FuelFraction() {
        return fuelControl.FuelFraction;
    }

    public float HealthFraction() {
        return health / healthCapacity;
    }

    public void ToggleLandingLock() {
        if (!GearDown()) {
            return;
        }

        if (landingJoint.enabled) {
            if (landingEventDelegate is LandingEventDelegate) {
                (landingEventDelegate as LandingEventDelegate).OnTakeoffFromObject(landingJoint.connectedBody.gameObject);
            }

            landingControl.UnlockLanding();
            Physics.Rigidbody().velocity = satelliteBody.Velocity();
        } else {
            bool locked = landingControl.TryLockLanding();
            if (locked) {
                if (landingEventDelegate is LandingEventDelegate) {
                    (landingEventDelegate as LandingEventDelegate).OnLandedAtObject(landingControl.CurrentLockedbody.gameObject);
                }

                InputUpdate(Vector3.zero); 
            }
        }
    }

    public bool CanLockLanding() {
        return !landingJoint.enabled && landingControl.SittingOnObject() != null;
    }

    bool ControlledByPlayer() {
        return SceneContext.instance.Player().currentVehicle == this;
    }
}
