﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterController : MonoBehaviour
{
    public ParticleSystem[] topParticles;
    public ParticleSystem[] rightParticles;
    public ParticleSystem[] bottomParticles;
    public ParticleSystem[] leftParticles;
    public ParticleSystem[] antiParticles;
    public ParticleSystem[] clockParticles;

    void Awake() {
        SetLevel(topParticles, 0f);
        SetLevel(bottomParticles, 0f);
        SetLevel(leftParticles, 0f);
        SetLevel(rightParticles, 0f);
        SetLevel(antiParticles, 0f);
        SetLevel(clockParticles, 0f);
    }

    public void SetThrusters(Vector3 input) {
        SetLevel(topParticles, -input.y);
        SetLevel(bottomParticles, input.y);
        SetLevel(leftParticles, input.x);
        SetLevel(rightParticles, -input.x);
        SetLevel(antiParticles, -input.z);
        SetLevel(clockParticles, input.z);
    }

    void SetLevel(ParticleSystem[] systems, float level) {
        level = Mathf.Clamp(level, 0f, 1f);
        
        
        foreach(ParticleSystem system in systems) {
            var emission = system.emission;
            emission.enabled = level > 1e-3;
            var main = system.main;
            main.startLifetime = level * 0.3f;
        }
    }
}
