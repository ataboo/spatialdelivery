﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAudioController : MonoBehaviour
{
    public AudioSource crash;
    public AudioSource die;
    public AudioSource thruster;
    public float thrusterMaxVol = 0.6f;
    float lastLevel;
    float pitchOffset;

    void Awake() {
        SetThrusterLevel(0);
        pitchOffset = Random.Range(0.9f, 1.1f);

    }

    void Update() {
        if (SharedContext.instance.IsPaused()) {
            SetThrusterLevel(0);
        }
    }

    public void PlayCrash(float damage) {
        crash.pitch = 1f + Random.Range(-0.1f, 0.1f);
        crash.volume = 0.2f + (damage) * 0.8f;

        crash.Play();
    }

    public void PlayDie() {
        SetThrusterLevel(0);
        die.Play();
    }

    public void SetThrusterLevel(float level, bool smooth = true) {
        level = Mathf.Clamp01(level);
        if (smooth) {
            level = Mathf.MoveTowards(lastLevel, level, Time.fixedDeltaTime * 6f);
        }
        lastLevel = level;
        thruster.pitch = (0.7f + level * 0.3f) * pitchOffset;
        thruster.volume = thrusterMaxVol * level;
    }
}
