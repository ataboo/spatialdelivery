using UnityEngine;
using System.Collections.Generic;

public class CollisionDetector: MonoBehaviour {
    public int stepCount = 10;
    public float timeStep = 1f;
    public Vector2 detectionBoxSize = new Vector2(30f, 30f);
    public CollisionContact NextCollision {
        get {
            return nextCollision;
        }
    }

    private VehicleController vehicle;
    private SatelliteBody sb;

    CollisionContact nextCollision;
    List<CollisionContact> candidates = new List<CollisionContact>();

    public bool CollisionDetected {
        get {
            return nextCollision != null;
        }
    }

    void Start() {
        vehicle = GetComponent<VehicleController>();
        sb = GetComponent<SatelliteBody>();
    }

    void OnDrawGizmosSelected() {
        float velDir = VelocityHeading();
        Quaternion velRotation = Quaternion.Euler(0, 0, velDir);

        Gizmos.color = Color.blue;
        Vector2 bl = transform.position - velRotation * Vector2.right * detectionBoxSize.x/2;
        Vector2 br = transform.position + velRotation * Vector2.right * detectionBoxSize.x/2;
        Vector2 tl = bl + (Vector2)(velRotation * Vector2.up * detectionBoxSize.y);
        Vector2 tr = br + (Vector2)(velRotation * Vector2.up * detectionBoxSize.y);

        Gizmos.DrawLine(bl, br);
        Gizmos.DrawLine(br, tr);
        Gizmos.DrawLine(tr, tl);
        Gizmos.DrawLine(tl, bl);

        Gizmos.color = Color.yellow;
        foreach(CollisionContact candidate in candidates) {
            Gizmos.DrawWireSphere(candidate.body.transform.position, candidate.body.CollisionRadius());
        }

        if (nextCollision != null) {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(nextCollision.body.transform.position, nextCollision.body.CollisionRadius());
        }
    }

    void FixedUpdate() {
        // TODO: run intermitent
        FindNextCollision();
    }

    void FindNextCollision() {
        FindCollisionCandidates();

        if (candidates.Count == 0) {
            nextCollision = null;
            return;
        }

        // right +ve
        Vector2 lateralHeading = -Vector2.Perpendicular(sb.Velocity().normalized);
        for(int i=0; i<stepCount; i++) {
            float timeOffset = timeStep * (float)i;
            double gameTime = SceneContext.GameTime + timeOffset; 

            Vector2 vehiclePosition = sb.Velocity() * timeOffset + (Vector2)sb.transform.position;

            var contact = ClosestCollidingContact(timeOffset, vehiclePosition, lateralHeading);

            if (contact != null) {
                nextCollision = contact;
                return;
            }
        }

        nextCollision = null;
    }

    float VelocityHeading() {
        if (sb == null || sb.Rigidbody() == null) {
            return transform.rotation.z;
        }

        return Vector2.SignedAngle(Vector2.up, sb.Rigidbody().velocity);
    }

    CollisionContact ClosestCollidingContact(float timeOffset, Vector2 vehiclePos, Vector2 vehicleLateralHeading) {
        CollisionContact closestContact = null;
        
        foreach(CollisionContact contact in candidates) {
            var predictedCollision = contact.ProjectAhead(timeOffset, vehicle, vehiclePos, vehicleLateralHeading);

            if (predictedCollision.type == PredictedCollision.Type.None) {
                continue;
            }

            if (closestContact == null || predictedCollision.range < closestContact.collision.range) {
                closestContact = contact;
            }
        }

        return closestContact;
    }

    List<CollisionContact> FindCollisionCandidates() {
        var velHeading = VelocityHeading();

        var colliders = Physics2D.OverlapBoxAll(vehicle.transform.position + Quaternion.Euler(0, 0, velHeading) * Vector2.up * detectionBoxSize.y/2, detectionBoxSize, velHeading);
        candidates.Clear();

        foreach(Collider2D col in colliders) {
            NewtonianBody body = col.GetComponent<NewtonianBody>();
            if (body != null && body != sb) {
                var contact = new CollisionContact(body);
                candidates.Add(contact);
            }
        }

        return candidates;
    }

    public class CollisionContact {
        public float rangeAtCollision;
        public Vector2 deltaPositionAtCollision;
        public Vector2 vehiclePosAtCollision;
        public Vector2 contactPosAtCollision;
        public NewtonianBody body;
        bool isRails;
        public PredictedCollision collision;

        public CollisionContact(NewtonianBody body) {
            this.body = body;
            isRails = body is RailsBody;
        }

        public PredictedCollision ProjectAhead(float timeAhead, VehicleController vehicle, Vector2 vehiclePos, Vector2 lateralHeading) {
            collision = PredictCollision(timeAhead, vehiclePos, vehicle, lateralHeading);

            return collision;
        }

        PredictedCollision PredictCollision(float timeAhead, Vector2 vehiclePosAtTime, VehicleController vehicle, Vector2 vehicleHeading) {
            
            float minRange = vehicle.CollisionRadius + body.CollisionRadius();
            
            deltaPositionAtCollision = ProjectDeltaPos(timeAhead, vehiclePosAtTime);
            rangeAtCollision = deltaPositionAtCollision.magnitude;
            
            if (deltaPositionAtCollision.magnitude < minRange) {
                var lateralProjection = AtaMath.Projections(deltaPositionAtCollision, vehicleHeading);
                var type = lateralProjection.scalar > 0 ? PredictedCollision.Type.Right : PredictedCollision.Type.Left;

                return new PredictedCollision(type, rangeAtCollision);
            }

            return new PredictedCollision(PredictedCollision.Type.None, rangeAtCollision);
        }

        Vector2 ProjectDeltaPos(float timeAhead, Vector2 vehiclePosAtTime) {
            Vector2 position = ProjectPosition(timeAhead); 
            
            return position - vehiclePosAtTime; 
        }

        Vector2 ProjectPosition(float timeAhead) {
            if (isRails) {
                var rails = body as RailsBody;
                return rails.PositionAtTime(SceneContext.GameTime + timeAhead);
            }

            return (Vector2)body.transform.position + body.Velocity() * timeAhead;
        }
    }

    public class PredictedCollision {
        public enum Type {
            Left,
            Right,
            None
        }
        public Type type;
        public float range;

        public PredictedCollision(Type type, float range) {
            this.type = type;
            this.range = range;
        }
    }
}