using UnityEngine;

public class VehicleLandingControl {
    
    private PhysicsBody currentLockedBody;
    private Config cfg;

    public PhysicsBody CurrentLockedbody {
        get {
            return currentLockedBody;
        }
    }

    public VehicleLandingControl(Config config) {
        this.cfg = config;

        if (cfg.startSittingOn != null) {
            JoinToBody(cfg.startSittingOn);
        }
    }

    public bool TryLockLanding() {
        if (currentLockedBody != null) {
            Debug.LogWarning("Tried to lock landing when already locked onto a body.");
            
            return true;
        }

        PhysicsBody sittingOnObject = SittingOnObject();
        if(sittingOnObject == null) {
            return false;
        }

        JoinToBody(sittingOnObject);

        return true;
    }

    public PhysicsBody SittingOnObject() {
        return cfg.legController.SittingOnObject();
    }

    public void UnlockLanding() {
        if (!cfg.landingJoint.enabled) {
            return;
        }

        cfg.legController.IndicateLanded(false);
        cfg.landingJoint.connectedBody = null;
        cfg.landingJoint.enabled = false;
        currentLockedBody = null;
        cfg.legController.RefreshColliders();
        cfg.fuelControl.SetRefueler(null);
    }

    private void JoinToBody(PhysicsBody physicsBody) {
        this.currentLockedBody = physicsBody;
        cfg.landingJoint.connectedBody = physicsBody.Rigidbody();
        cfg.landingJoint.enabled = true;
        cfg.fuelControl.SetRefueler(physicsBody.GetComponent<RefuelController>());
        cfg.legController.IndicateLanded(true);
    }

    public void SetLandingLegIndicationEnabled(bool enabled) {
        cfg.legController.enabled = enabled;
        cfg.legController.UpdateTouchingIndication();
    }

    public struct Config {
        public Joint2D landingJoint;
        public LanderLegController legController;
        public VehicleFuelController fuelControl;
        public PhysicsBody startSittingOn;
        public OrbitalOverlay overlay;
    }
}