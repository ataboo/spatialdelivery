﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulkCarrierController : StationController, Trigger2DDelegate
{
    public Animator topDoor;
    public Animator bottomDoor;
    public bool bottomStartOpen = false;
    public bool topStartOpen = false;
    public List<Payload> topStorage = new List<Payload>();
    public GameObject topContainerSpawn;
    public Collider2D[] doorColliders;
    public Collider2D[] hullColliders;
    public DelegatingTrigger containerProximityTrigger;

    private Animator topAnimator;
    private FixedJoint2D topJoint;
    private PayloadController lastTopPayload;

    void Start() {
        containerProximityTrigger.triggerDelegate = this;
        topAnimator = topContainerSpawn.GetComponent<Animator>();
        topJoint = topContainerSpawn.GetComponent<FixedJoint2D>();
        SetDoorState(topDoor, topStartOpen, false);
        SetDoorState(bottomDoor, bottomStartOpen, false);
        SpawnTopPayload();
    }

    // void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.Alpha2)) {
    //         SetDoorState(topDoor, !DoorOpening(topDoor));
    //     }       

    //     if (Input.GetKeyDown(KeyCode.Alpha3)) {
    //         SetDoorState(bottomDoor, !DoorOpening(bottomDoor));
    //     }

    //     if (Input.GetKeyDown(KeyCode.Alpha4)) {
    //         SpawnTopPayload();
    //     }
    // }

    void SpawnTopPayload() {
        if (topStorage.Count == 0) {
            return;
        }

        var nextPayload = topStorage[0];
        topStorage.RemoveAt(0);

        GameObject payloadGO = nextPayload.Spawn(topContainerSpawn.transform);
        payloadGO.transform.localPosition = Vector2.zero;
        payloadGO.transform.localRotation = Quaternion.Euler(Vector3.zero);
        var payloadController = payloadGO.GetComponent<PayloadController>();
        payloadController.AttachToBody(GetComponent<OrbitingBody>());
        var payloadCols = payloadController.GetColliders();
        IgnoreHullCollision(payloadCols, true);
        IgnoreDoorCollision(payloadCols, true);
        

        topAnimator.Play("BulkLaunch", -1, 0f);
        lastTopPayload = payloadController;
    }

    // Open or close a cargo door animator.
    // Passing false to `animate` snaps to the end of the transition.
    void SetDoorState(Animator doorAnimator, bool open, bool animate = true) {
        doorAnimator.SetFloat("direction", open ? 1f : -1f);

        if (animate) {
            // After the non-looping door animation is done, the animator keeps playing past the normalized time so we need to clamp it to start the animation right away.
            doorAnimator.Play("BulkDoor", -1, Mathf.Clamp01(doorAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime));
        } else {
            doorAnimator.Play("BulkDoor", -1, open ? 1f : 0f);
        }
    }

    // Determine if a cargo door animator is opening or closing.
    bool DoorOpening(Animator doorAnimator) {
        return doorAnimator.GetFloat("direction") == 1f;
    }

    public void IgnoreHullCollision(Collider2D[] otherColliders, bool ignore) {
        foreach(Collider2D otherCol in otherColliders) {
            foreach(Collider2D hullCol in hullColliders) {
                Physics2D.IgnoreCollision(otherCol, hullCol, ignore);
            }
        }
    }

    public void IgnoreDoorCollision(Collider2D[] otherColliders, bool ignore) {
        foreach(Collider2D otherCol in otherColliders) {
            foreach(Collider2D doorCol in doorColliders) {
                Physics2D.IgnoreCollision(otherCol, doorCol, ignore);
            }
        }
    }

    public void DelTriggerEnter2D(Collider2D col, DelegatingTrigger sender)
    {
        if (sender == containerProximityTrigger) {
            var payload = col.GetComponent<PayloadController>();
            if (payload != null && col == payload.MainCollider()) {
                OnPayloadProximityEnter(payload);
            }
        }
    }

    public void DelTriggerExit2D(Collider2D col, DelegatingTrigger sender)
    {
        if (sender == containerProximityTrigger) {
            var payload = col.GetComponent<PayloadController>();
            if (payload != null && col == payload.MainCollider() && !col.attachedRigidbody.isKinematic) {
                OnPayloadProximityExit(payload);
            }
        }
    }

    void OnPayloadProximityEnter(PayloadController payload) {
        //
    }

    void OnPayloadProximityExit(PayloadController payload) {
        if (lastTopPayload == payload) {
            lastTopPayload = null;
            SpawnTopPayload();
        }
    }
}
