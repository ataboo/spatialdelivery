using UnityEngine;

// RailsBody Unit
public class StationController: UnitController {
    RailsBody railsBody;


    override protected void Awake() {
        base.Awake();
        
        railsBody = GetComponent<RailsBody>();
    }
}