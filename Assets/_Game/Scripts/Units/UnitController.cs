﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class UnitController: MonoBehaviour
{
    PlayerWeaponController playerWeapons;
    NPCWeaponController npcWeapons;
    protected PhysicsBody physics;
    public float collisionRadius = 5f;
    public float healthCapacity = 100f;
    public float health;
    public bool immortal = false;
    public float impactScalar = 1f;
    public float legImpactAbsorption = 5f;
    public VehicleCarcassController carcassPrefab;
    protected ShipAudioController audioController;
    public FactionOption factionOption;
    Collider2D bodyCollider;

    public Faction Faction {
        get {
            return FactionManager.instance.GetFaction(factionOption);
        }
    }

    public PlayerWeaponController PlayerWeapons {
        get {
            return playerWeapons;
        }
    }

    public NPCWeaponController NpcWeapons {
        get {
            return npcWeapons;
        }
    }

    public PhysicsBody Physics {
        get {
            return physics;
        }
    }

    public float CollisionRadius {
        get {
            return collisionRadius;
        }
    }

    protected virtual void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, collisionRadius);
    }

    protected virtual void Awake() {
       this.playerWeapons = GetComponent<PlayerWeaponController>();
       this.npcWeapons = GetComponent<NPCWeaponController>();
       this.physics = GetComponent<PhysicsBody>();
       this.audioController = GetComponentInChildren<ShipAudioController>();
       bodyCollider = GetComponent<Collider2D>();
       health = healthCapacity; 
    }

    public void SwitchToNPCWeapons() {
        if (PlayerWeapons != null) {
            playerWeapons.enabled = false;
            NpcWeapons.enabled = true;
            NpcWeapons.Initialize();
        }
    }

    public void SwitchToPlayerWeapons() {
        if (PlayerWeapons != null) {
            npcWeapons.enabled = false;
            playerWeapons.enabled = true;
            playerWeapons.Initialize();
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (immortal) {
            return;
        }

        float velocity = GetColliderVelocity(collision);
        TakeCollisionDamage(velocity, collision.otherCollider == bodyCollider);
    }

    float GetColliderVelocity(Collision2D collision) {
        var rotating = collision.gameObject.GetComponent<RotatingBody>();
        if (rotating != null) {
            return physics.GroundSpeed(rotating).magnitude;
        }

        PhysicsBody otherBody = collision.gameObject.GetComponent<PhysicsBody>();
        if (otherBody != null) {
            return (physics.Velocity() - otherBody.Velocity()).magnitude;
        }

        return collision.relativeVelocity.magnitude;
    }

    protected virtual void TakeCollisionDamage(float velocity, bool hitBody) {
        float impact = velocity * velocity * impactScalar;

        if (!hitBody) {
            impact -= legImpactAbsorption;
        } else {
            impact -= 0.1f;
        }

        if (impact > 0) {
            this.TakeDamage(impact);
        }
    }

    public void TakeDamage(float damage) {
        if (!immortal) {
            health = Mathf.Max(0f, health - damage);
            // Debug.LogFormat("Took {0} damage, {1} left", damage, health);
        } else {
            Debug.LogFormat("{0} is immortal", name);
        }

        if (audioController != null) {
            audioController.PlayCrash(Mathf.Min(1, damage / 100f));
        } else {
            Debug.LogWarningFormat("{0} is missing an audio controller", name);
        }

        if (health == 0f) {
            Die();
        }
    }

    protected virtual void Die() {
        var carcass = GameObject.Instantiate<VehicleCarcassController>(carcassPrefab, transform.position, transform.rotation);
        
        carcass.SetVelocity(physics.Velocity());
        
        if (audioController != null) {
            audioController.SetThrusterLevel(0, false);
            audioController.gameObject.transform.SetParent(carcass.transform);
            audioController.PlayDie();
        }
        
        GameObject.Destroy(gameObject);
    }
}
