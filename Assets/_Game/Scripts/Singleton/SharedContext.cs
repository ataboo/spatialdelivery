using UnityEngine;
using System.Collections.Generic;
using Atasoft.Translation;

public class SharedContext : MonoBehaviour {
    public const float GRAV_CONST = 1;
    public static SharedContext instance;
    private float[] timeScales = new float[]{1f, 2f, 4f, 8f};
    private int timeScaleIdx = 0;
    public GameObject toastPrefab;
    public GameObject toastHolder;

    private static Translator translations;

    public static void Quit() {
        if (Application.isEditor) {
            UnityEditor.EditorApplication.isPlaying = false;
        } else {
            Application.Quit();
        }
    }

    void Awake() {
        if (instance != null) {
            Debug.LogError("There should only be one SharedContext instance");
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    public void TogglePause() {
        SetPause(!IsPaused());
    }

    public void SetPause(bool paused) {
        SetTimeScale(paused ? 0f : 1f);
    }

    public bool IsPaused() {
        return Time.timeScale == 0f;
    }

    void SetTimeScale(float scale) {
        Time.timeScale = scale;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    public void IncreaseTimeScale() {
        timeScaleIdx = Mathf.Clamp(timeScaleIdx + 1, 0, timeScales.Length-1);
        SetTimeScale(timeScales[timeScaleIdx]);
    }

    public void DecreaseTimeScale() {
        timeScaleIdx = Mathf.Clamp(timeScaleIdx - 1, 0, timeScales.Length-1);
        SetTimeScale(timeScales[timeScaleIdx]);
    }

    public float TimeCompressionFraction() {
        return ((float)timeScaleIdx + 1f) / (float)timeScales.Length;
    }

    public static string GetString(string stringKey) {
        if (translations == null) {
            translations = new Translator();
        }

        return translations.GetString(stringKey);
    }

    public static QuestData GetQuest(string questId) {
        if (translations == null) {
            translations = new Translator();
        }

        return translations.GetQuest(questId);
    }

    public static GameObject ShowToast(string message, float lifeSpan = 3f) {
        ClearToasts();

        var toastObj = GameObject.Instantiate(instance.toastPrefab, instance.toastHolder.transform);
        var toastMessage = toastObj.GetComponent<ToastMessage>();
        toastMessage.message = message;
        toastMessage.lifeSpan = lifeSpan;

        return toastObj;
    }

    public static void ClearToasts() {
        foreach(Transform child in instance.toastHolder.transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    public class Scenes {
        public const string CargoUnload = "CargoUnload";
        public const string MainMenu = "MainMenu";
    }
}