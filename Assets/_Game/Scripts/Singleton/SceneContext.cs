﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneContext : MonoBehaviour
{
    public GameObject redContainerPrefab;
    private PlayerController player;
    private CameraController cameraController;
    public static SceneContext instance;
    public GameObject pauseMenu;
    private double gameTime;
    public QuestEventDelegate sceneStartTrigger;
    
    public static double GameTime {
        get {
            return instance == null ? Time.time : instance.gameTime;
        }
    }

    void Awake() {
        if (instance != null) {
            Debug.LogError("There should only be one SceneContext instance");
            Destroy(gameObject);
        } else {
            gameTime = 0d;
            instance = this;
        }
    }

    void Start() {
        SharedContext.ClearToasts();
        sceneStartTrigger.Trigger();
    }

    void Update() {
        if(Input.GetButtonDown("Cancel")) {
            SetPause(!SharedContext.instance.IsPaused());
        }
    }

    void FixedUpdate() {
        gameTime += Time.fixedDeltaTime;
    }

    public PlayerController Player() {
        if (player == null) {
            player = GetComponent<PlayerController>();
        }

        return player;
    }

    public CameraController MainCamController() {
        if (cameraController == null) {
            GameObject camObj = GameObject.FindGameObjectWithTag("MainCamera");
            if (camObj != null) {
                cameraController = camObj.GetComponent<CameraController>();
            }
        }

        return cameraController;
    }

    public WeaponBarController WeaponBarController() {
        return GameObject.FindObjectOfType<WeaponBarController>();
    }

    public Camera MainCamera() {
        if (MainCamController() != null) {
            return MainCamController().mainCamera;
        }

        return null;
    }

    public VehicleController PlayerVehicle() {
        if (Player() == null) {
            return null;
        }

        return Player().currentVehicle;
    }

    public GameObject GetPayloadPrefab(Payload.Type type) {
        switch (type) {
            case Payload.Type.RED_CARGO:
                return redContainerPrefab;
            default:
                throw new System.NotImplementedException();
        }
    }

    public void RestartScene() {
        SharedContext.ClearToasts();
        SharedContext.instance.SetPause(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitScene() {
        SharedContext.ClearToasts();
        SharedContext.instance.SetPause(false);
        SceneManager.LoadScene(SharedContext.Scenes.MainMenu);
    }

    public void ResumeScene() {
        SetPause(false);
    }

    private void SetPause(bool paused) {
        SharedContext.instance.SetPause(paused);
        pauseMenu.SetActive(paused);
    }
}
