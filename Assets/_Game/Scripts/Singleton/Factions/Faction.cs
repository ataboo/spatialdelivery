using System;
using UnityEngine;
using System.Collections.Generic;


[Serializable]
public class Faction {
    string key;
    public FactionOption option;
    public Color iconColor;
    public List<FactionOption> allies;

    public string Key {
        get {
            return key;
        }
    }

    public int Index {
        get {
            return (int)option;
        }
    }

    public string FullKey {
        get {
            return "faction." + key;
        }
    }

    public Faction(FactionOption option, Color iconColor, List<FactionOption> allies = null) {
        this.option = option;
        this.iconColor = iconColor;
        this.key = option.Name();
        if (allies == null) {
            this.allies = new List<FactionOption>();
        } else {
            this.allies = allies;
        }

    }

    public bool AlliedTo(Faction other) {
        if (option == other.option) {
            return true;
        }

        if (other.option == FactionOption.Neutral) {
            return true;
        }

        return allies.Contains(other.option);
    }
}

[Serializable]
public enum FactionOption {
    Player,
    Colony,
    Pirate,
    Alien,
    Neutral
}

public static class FactionOptionExtensions {
    public static string Name(this FactionOption option) {
        return Enum.GetName(typeof(FactionOption), option);
    }
}
