﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FactionManager : MonoBehaviour
{
    public static FactionManager instance;
    static string[] factionKeys;

    public Faction[] factions = new Faction[]{
      new Faction(FactionOption.Player, Color.green, new List<FactionOption>{FactionOption.Colony}),
      new Faction(FactionOption.Colony, new Color(0.2f, 0.3f, 0.7f), new List<FactionOption>{FactionOption.Player}),
      new Faction(FactionOption.Pirate, Color.red),
      new Faction(FactionOption.Alien, Color.magenta),
      new Faction(FactionOption.Neutral, Color.white),
    };

    void Awake() {
        if (instance != null) {
            Debug.LogError("There should only be one FactionManager instance");
            Destroy(gameObject);
        } else {
            instance = this;
        }
    }

    public Faction GetFaction(FactionOption option) {
        foreach(Faction faction in factions) {
            if (faction.option == option) {
                return faction;
            }
        }
        
        throw new System.NotImplementedException(option.Name());
    }
}
