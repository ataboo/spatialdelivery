﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class QuestEntryController : MonoBehaviour
{
    public Text title;
    public Text description;
    private QuestData quest;

    public void SetQuest(QuestData quest) {
        this.quest = quest;
        title.text = quest.title;
        RefreshDescriptionText();
    }

    public void RefreshDescriptionText() {
        var builder = new StringBuilder();

        builder.Append(quest.description);

        if (quest.trackedItems.Count > 0) {
            builder.Append("\n");
            foreach(QuestDataItem item in quest.trackedItems) {
                builder.Append("\n");
                string noun = item.total > 1 ? item.plural : item.singular;
                builder.Append(string.Format("{0}: {1} / {2}", noun, item.progress, item.total));
            }
        }

        description.text = builder.ToString();
    }

    public void SetItemProgress(string itemId, int progress) {
        UpdateItem(itemId, progress);

        RefreshDescriptionText();
    }

    void UpdateItem(string itemId, int progress) {
        foreach(QuestDataItem item in quest.trackedItems) {
            if (item.id == itemId) {
                item.progress = progress;
        
                return;
            }
        }
    
        Debug.LogErrorFormat("Failed to find item for quest {0} with id {1}", quest.id, itemId);
    }
}
