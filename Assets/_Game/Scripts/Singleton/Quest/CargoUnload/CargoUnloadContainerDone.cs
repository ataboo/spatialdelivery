using UnityEngine;
using System.Collections;

public class CargoUnloadContainerDone : QuestEventDelegate, LandingEventDelegate
{
    private bool doneDelivery = false;
    private bool landed = false;

    public override void Trigger(params GameObject[] contextObjects)
    {
        StartCoroutine(ContainerDoneSequence());
    }

    IEnumerator ContainerDoneSequence() {
        SharedContext.ShowToast("Well done!... Go home for space cocao.", 3f);
        doneDelivery = true;

        if (landed) {
            yield return BaseLandingSequence();
        }

        yield return null;
    }

    IEnumerator BaseLandingSequence() {
        SharedContext.ShowToast("Welcome home!...  Pflfudding?", 3f);

        yield return new WaitForSeconds(4f);

        SceneContext.instance.QuitScene();
    }

    public void OnLandedAtObject(GameObject baseObject)
    {
        if (baseObject.GetComponent<RefuelController>() == null) {
            return;
        }

        landed = true;

        if (doneDelivery) {
            StartCoroutine(BaseLandingSequence());        
        }
    }

    public void OnTakeoffFromObject(GameObject baseObject)
    {
        landed = false;
    }
}