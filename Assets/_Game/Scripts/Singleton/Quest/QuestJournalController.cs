﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class QuestJournalController : MonoBehaviour
{
    public GameObject entryPrefab;
    private Dictionary<string, QuestEntryController> trackedQuests = new Dictionary<string, QuestEntryController>();
    private Image panelImage;
    
    public static QuestJournalController instance;

    void Awake() {
        if (instance != null) {
            Debug.LogError("There should only be one QuestJournalController instance");
            Destroy(gameObject);
        } else {
            instance = this;
        }
    }

    void Start() {
        panelImage = GetComponent<Image>();
    }

    public void StartQuest(string questId) {
        var quest = SharedContext.GetQuest(questId);
        quest.Reset();

        GameObject logEntry = GameObject.Instantiate(entryPrefab);
        logEntry.transform.SetParent(gameObject.transform);
        logEntry.transform.localScale = new Vector3(1, 1, 1);
        var questEntry = logEntry.GetComponent<QuestEntryController>();
        questEntry.SetQuest(quest);

        trackedQuests[questId] = questEntry;

        UpdatePanelVisibility();
    }

    public void EndQuest(string questId) {
        var questEntry = trackedQuests[questId];

        GameObject.Destroy(questEntry.gameObject);
        trackedQuests.Remove(questId);

        UpdatePanelVisibility();
    }

    public void UpdateQuestItemProgress(string questId, string itemId, int progress) {
        var questEntry = trackedQuests[questId];

        questEntry.SetItemProgress(itemId, progress);
    }

    public void RefreshQuestDescription(string questId) {
        trackedQuests[questId].RefreshDescriptionText();
    }

    void UpdatePanelVisibility() {
        panelImage.enabled = trackedQuests.Count > 0;
    }
}

[Serializable]
public struct QuestData {
    public string id;
    public string title;
    public string description;
    public List<QuestDataItem> trackedItems;

    public void Reset() {
        foreach(QuestDataItem item in trackedItems) {
            item.progress = 0;
        }
    }

    public QuestDataItem GetItem(string itemId) {
        foreach(QuestDataItem item in trackedItems) {
            if (item.id == itemId) {
                return item;
            }
        }

        return null;
    }
}

[Serializable]
public class QuestDataItem {
    public string id;
    public string singular;
    public string plural;
    public int total;
    public int progress;
}

[Serializable]
public class QuestDataList {
    public List<QuestData> questList;
}
