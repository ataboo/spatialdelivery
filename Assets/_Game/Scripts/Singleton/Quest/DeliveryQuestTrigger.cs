﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryQuestTrigger : QuestEventDelegate
{
    public string questId;
    public string questItemId;
    public Payload.Type[] payloadTypeWhitelist;
    public QuestEventDelegate completionDelegate;
    private QuestData quest;
    private QuestDataItem trackedItem;

    void Awake() {
        quest = SharedContext.GetQuest(questId);
        trackedItem = quest.GetItem(questItemId);
    }

    public override void Trigger(params GameObject[] contextObjects)
    {
        if (contextObjects.Length != 1) {
            Debug.LogError("DeliveryQuestTrigger expected 1 context object.");
            return;
        }

        var delivery = contextObjects[0];

        if (PayloadWhitelisted(delivery)) {
            trackedItem.progress++;

            QuestJournalController.instance.RefreshQuestDescription(quest.id);

            if (trackedItem.progress >= trackedItem.total) {
                completionDelegate.Trigger();
            }
        }
    }

    bool PayloadWhitelisted(GameObject payloadObject) {
        var payloadController = payloadObject.GetComponent<PayloadController>();
        if (payloadController != null) {
            foreach(Payload.Type whiteListType in payloadTypeWhitelist) {
                if (payloadController.type == whiteListType) {
                    return true;
                }
            }        
        }
    
        return false;
    }
}
