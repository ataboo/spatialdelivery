using UnityEngine;

abstract public class QuestEventDelegate : MonoBehaviour {
    public abstract void Trigger(params GameObject[] contextObjects);
}