﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Apply gravitational forces from Orbital Bodies to physics and visualize path.
public class GravitationalController : MonoBehaviour
{
    private List<OrbitalBodyContact> contacts = new List<OrbitalBodyContact>();
    private Dictionary<int, SatelliteContact> satellites = new Dictionary<int, SatelliteContact>();
    private LayerMask orbitalLayer;
    private OrbitalOverlay orbitalOverlay;
    

    void Start() {
        UpdateOrbitalBodies();
    }

    void FixedUpdate() {
        // UpdateOrbitalBodies();

        var garbageKeys = new List<int>();
        foreach(KeyValuePair<int, SatelliteContact> entry in satellites) {
            if (!entry.Value.IsAlive()) {
                garbageKeys.Add(entry.Key);
                continue;
            }

            entry.Value.ApplyOrbitalForces(contacts);
        }

        foreach(int id in garbageKeys) {
            satellites.Remove(id);
        }
    }

    public void AddSatellite(SatelliteBody satellite) {
        satellites.Add(satellite.GetInstanceID(), new SatelliteContact(satellite));
    }

    public void RemoveSatellite(SatelliteBody satellite) {
        satellites.Remove(satellite.GetInstanceID());
    }

    public List<OrbitalBodyContact> GetOrbitalBodies() {
        return contacts;
    }

    private void UpdateOrbitalBodies() {
        contacts.Clear();
        var bodies = GameObject.FindGameObjectsWithTag("OrbitalBody");
        foreach(GameObject gObj in bodies) {
            RailsBody body = gObj.GetComponent<RailsBody>();
            if (body == null) {
                Debug.LogErrorFormat("GameObject {0} should have an OrbitalBody component if tagged.", gObj.name);
                continue;
            }

            contacts.Add(new OrbitalBodyContact(body));
        }
    }    
}

public class OrbitalBodyContact {
    public RailsBody body;

    public OrbitalBodyContact(RailsBody body) {
        this.body = body;
    }

    private Vector2 BearingNow(Vector2 position) {
        return (Vector2)body.transform.position - position;
    }

    private Vector2 BearingAtTime(Vector2 position, double time) {
        return (Vector2)body.PositionAtTime(time) - position;
    }

    private float GravAccel(Vector2 bearing) {
        return SharedContext.GRAV_CONST * body.mass / bearing.sqrMagnitude;
    }

    public (Vector2 bearing, Vector2 accel) StatsNow(Vector2 satellitePosition) {
        Vector2 bearing = BearingNow(satellitePosition);
        return (bearing, bearing.normalized  * GravAccel(bearing));
    }

    public (Vector2 position, Vector2 bearing, Vector2 accel) StatsAtTime(Vector2 satellitePosition, double time) {
        Vector2 pos = (Vector2)body.PositionAtTime(time);
        Vector2 bearing = pos - satellitePosition;
        Vector2 accel = bearing.normalized * GravAccel(bearing);

        return (pos, bearing, accel);
    }
}

public class SatelliteContact {
    public SatelliteBody body;
    public Rigidbody2D rb;

    public SatelliteContact(SatelliteBody body) {
        this.body = body;
        this.rb = body.GetComponent<Rigidbody2D>();
    }

    public bool IsAlive() {
        return body != null;
    }

    public void ApplyOrbitalForces(List<OrbitalBodyContact> orbitals) {
        if (!body.enabled) {
            return;
        }
        float closestDistSqr = Mathf.Infinity;
        RailsBody closestBody = null;

        foreach(OrbitalBodyContact orbital in orbitals) {
            float distSqr = (body.transform.position - orbital.body.transform.position).sqrMagnitude - (orbital.body.approxRadius * orbital.body.approxRadius);
            if (distSqr < closestDistSqr) {
                closestDistSqr = distSqr;
                closestBody = orbital.body;
            }

            if(!body.ignoreGravity) {
                Vector2 force = orbital.StatsNow(body.transform.position).accel * rb.mass;
                rb.AddForce(force); 
            }
        }

        body.SetOrbitCenter(closestBody);
    }
}

