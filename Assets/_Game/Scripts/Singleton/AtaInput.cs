public class AtaInput {
    public const string VERT = "Vertical";
    public const string HORIZ = "Horizontal";
    public const string ROTATE = "Rotate";
    public const string FORK_GRAB = "Land";
    public const string GEAR = "Gear";
    public const string LAND = "Land";
    public const string SWAP_VEHICLE = "SwapVehicle";
    public const string TIME_UP = "TimeUp";
    public const string TIME_DOWN = "TimeDown";
    public const string FIRE = "Fire1";
    public const string WEAPON_1 = "Weapon1";
    public const string LOCK = "LockTarget";
}